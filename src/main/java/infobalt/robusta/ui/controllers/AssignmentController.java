package infobalt.robusta.ui.controllers;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.primefaces.event.SelectEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Member.MemberStatus;
import infobalt.robusta.entities.Project;
import infobalt.robusta.entities.Sprint;
import infobalt.robusta.model.AssignmentModel;
import infobalt.robusta.model.AssignmentSearchCriterias;
import infobalt.robusta.model.MemberModel;
import infobalt.robusta.model.ProjectModel;
import infobalt.robusta.model.SprintModel;
import infobalt.robusta.service.AssignmentService;
import infobalt.robusta.service.MemberService;
import infobalt.robusta.service.SprintService;

public class AssignmentController implements Serializable {

	private static final long serialVersionUID = -1389684115871062390L;

	static final Logger logger = LoggerFactory.getLogger(AssignmentController.class);

	private AssignmentSearchCriterias assCriteria = new AssignmentSearchCriterias();
	private ProjectModel projectModel;
	private AssignmentModel assignmentModel;
	private AssignmentService assignmentService;
	private MemberModel memberModel;
	private MemberService memberService;
	private SprintService sprintService;
	private SprintModel sprintModel;

	public void addNewAssignment() {
		assignmentModel.setCurrentAssignment(new Assignment());
		assignmentModel.setAssigneeId(null);
	}

	public String removeFromSprint(Assignment assignment) {
		Sprint sprint = assignment.getSprint();
		logger.debug("Assignment's(ID{}) sprint is found", assignment.getId());
		sprint.getSprintAssignments().remove(assignment);
		logger.debug("Assignment(ID{}) is removed from sprint's(ID{}) list", assignment.getId(), sprint.getId());
		assignment.setSprint(null);
		sprintService.saveSprint(sprint);
		sprintModel.setSprint(sprint);
		assignmentService.save(assignment);
		sprintService.updateAndSaveBurndownHistory(sprint);

		return "sprintView";
	}

	public String addToSprint(Assignment assignment) {
		Sprint sprint = sprintModel.getSprint();
		logger.debug("Sprint(ID{}) is found", sprint.getId());
		if (assignment == null || sprint == null) {
			logger.debug("Assignment(ID{}) is not found", assignment.getId());
			return " ";
		}
		sprint.getSprintAssignments().add(assignment);
		logger.debug("Assignment(ID{}) is added to sprint's(ID{}) list", assignment.getId(), sprint.getId());
		assignment.setSprint(sprint);
		sprintService.saveSprint(sprint);
		assignmentService.save(assignment);
		sprintService.updateAndSaveBurndownHistory(sprint);
		return "sprintView";
	}

	public void update(Assignment assignment) {
		assignmentModel.setEditedAssignment(assignment);
		assignmentModel.setCurrentAssignment(assignment);
		if (assignment.getAssignee() != null) {
			assignmentModel.setAssigneeId(assignment.getAssignee().getId());
		} else {
			assignmentModel.setAssigneeId(null);
		}
	}

	/**
	 * Saves or updates the assignment setting input values to it such as dates,
	 * assignee if any, status and other *
	 */
	public void save() throws ValidatorException {
		Assignment assignment = assignmentModel.getCurrentAssignment();
		Member assignee = null;

		if (assignmentModel.getAssigneeId() != null) {
			assignee = memberService.findMemberById(assignmentModel.getAssigneeId());
		}
		Member creator = memberModel.getLoggedMember();
		Date currentDate = new Date();
		assignment.setProject(projectModel.getManagedPro());
		if (assignment.getStatus() == null) {
			assignment.setStatus(AssignmentStatus.TODO);
			logger.debug("Assignment's(ID{}) status is set", assignment.getId());
		}
		if (assignmentModel.getEditedAssignment() != null) {
			if (assignment.getStatus().equals(AssignmentStatus.DONE)
					&& (!assignmentModel.getEditedAssignment().equals(AssignmentStatus.DONE))) {
				assignment.setDoneAuthor(assignee);
				assignment.setDoneDate(currentDate);
			}
			if (!(assignment.getStatus().equals(AssignmentStatus.DONE))
					&& assignmentModel.getEditedAssignment().equals(AssignmentStatus.DONE)) {
				assignment.setDoneAuthor(null);
				assignment.setDoneDate(null);
			}
		}
		assignmentModel.setEditedAssignment(null);

		assignment.setCreator(creator);
		logger.debug("Assignment creator is set");
		assignment.setAssignee(assignee);
		logger.debug("Assignment assignee is set");

		if (assignment.getCreationDate() == null) {
			assignment.setCreationDate(currentDate);
			logger.debug("Assignment creation date is set");
		}
		assignment.setEditedDate(currentDate);
		assignmentService.save(assignment);
		if (assignment.getSprint() != null) {
			sprintService.updateAndSaveBurndownHistory(assignment.getSprint());
		}
		assignmentModel.setCurrentAssignment(new Assignment());
	}

	public void delete(Assignment assignment) {
		assignmentService.delete(assignment);
	}

	public void cancel() {
		assignmentModel.setCurrentAssignment(null);
	}

	public List<Assignment> findAssignmentsByManagedProject() {
		return assignmentService.findAllByProject(projectModel.getManagedPro());
	}

	/**
	 * Method checks if a certain assignment belongs to any sprint.
	 * 
	 * @param assignment
	 * @return yes or no
	 */
	public boolean doesAssignmentHaveSprint(Assignment assignment) {
		if (assignment.getSprint() != null) {
			return true;
		}
		return false;
	}

	public List<Assignment> loadWorkingAssignments() {
		Member loggedWorker = memberModel.getLoggedMember();
		return assignmentService.findAssignmentsByAsignee(loggedWorker);
	}

	/**
	 * Methods redirects in a detailed view of a certain assignment in its
	 * project.
	 * 
	 * @param assignment
	 * @return page view with all assignment's data
	 */
	public String viewAssignment(Assignment assignment) {
		assignmentModel.setCurrentAssignment(assignment);
		projectModel.setManagedPro(assignment.getProject());
		return "assignmentView";
	}

	/**
	 * If any changes are made to assignment, the date of the changes is set as
	 * "edited" date.
	 */
	private void updateEditedDateAndSave() {
		Date currentDate = new Date();
		assignmentModel.getCurrentAssignment().setEditedDate(currentDate);
		assignmentService.save(assignmentModel.getCurrentAssignment());
	}

	public void setStatusToDo() {
		Assignment assToSetStatus = assignmentModel.getCurrentAssignment();
		if (assToSetStatus.getStatus().equals(AssignmentStatus.DONE)) {
			assToSetStatus.setDoneAuthor(null);
			assToSetStatus.setDoneDate(null);
		}

		assToSetStatus.setStatus(AssignmentStatus.TODO);
		logger.debug("Assignment's status changed to TODO");
		updateEditedDateAndSave();
		if (assToSetStatus.getSprint() != null) {
			sprintService.updateAndSaveBurndownHistory(assToSetStatus.getSprint());
		}
	}

	public void setStatusInProgress() {
		Assignment assToSetStatus = assignmentModel.getCurrentAssignment();
		if (assToSetStatus.getStatus().equals(AssignmentStatus.DONE)) {
			assToSetStatus.setDoneAuthor(null);
			assToSetStatus.setDoneDate(null);
		}

		assToSetStatus.setStatus(AssignmentStatus.INPROGRESS);
		logger.debug("Assignment's status changed to INRPOGRESS");
		updateEditedDateAndSave();
		if (assToSetStatus.getSprint() != null) {
			sprintService.updateAndSaveBurndownHistory(assToSetStatus.getSprint());
		}
	}

	public void setStatusDone() {
		Assignment assToSetStatus = assignmentModel.getCurrentAssignment();
		if (!assToSetStatus.getStatus().equals(AssignmentStatus.DONE)) {
			assToSetStatus.setDoneAuthor(assToSetStatus.getAssignee());
			assToSetStatus.setDoneDate(new Date());
		}
		assToSetStatus.setStatus(AssignmentStatus.DONE);
		logger.debug("Assignment's status changed to DONE");
		updateEditedDateAndSave();
		if (assToSetStatus.getSprint() != null) {
			sprintService.updateAndSaveBurndownHistory(assToSetStatus.getSprint());
		}
	}

	public void toggleSearchPanel() {
		assignmentModel.setVisible(!assignmentModel.getVisible());
	}

	/**
	 * Complete search by various criteria in all projects user is assigned to.
	 * 
	 * @return list of assignments found by entered criteria
	 */
	public List<Assignment> searchByEverything() {
		Member loggedWorker = memberModel.getLoggedMember();
		assignmentModel
				.setFoundAssignments(assignmentService.findAssignmentsByCriterias(assCriteria, loggedWorker.getId()));
		logger.debug("Assignments found by criterias set as search results");
		assignmentModel.setVisible(!assignmentModel.getVisible());
		return assignmentModel.getFoundAssignments();
	}

	/**
	 * Complete search by various criteria in a certain project.
	 * 
	 * @return list of assignments found by entered criteria
	 */
	public List<Assignment> searchByEverythingInProject() {
		Member loggedWorker = memberModel.getLoggedMember();
		Project project = projectModel.getManagedPro();
		assCriteria.setSpecificProject(project);
		assignmentModel
				.setFoundAssignments(assignmentService.findAssignmentsByCriterias(assCriteria, loggedWorker.getId()));
		assignmentModel.setVisible(!assignmentModel.getVisible());
		assCriteria.setSpecificProject(null);
		return assignmentModel.getFoundAssignments();
	}

	/**
	 * Creates set with all assignment's data shown in a grid in assignment's
	 * view in order to export all the fields to CSV file
	 * 
	 * @return Set of strings with key and value
	 */
	public Set<Entry<String, String>> createAssignmentPropertyMap() {

		Assignment assignmentToFile = assignmentModel.getCurrentAssignment();

		Map<String, String> dataToFile = new HashMap<String, String>();
		dataToFile.put("Assignment ID:", assignmentToFile.getId().toString());
		dataToFile.put("Assignment name:", assignmentToFile.getName());
		dataToFile.put("Assignment was created by:", assignmentToFile.getCreator().getFullName());
		dataToFile.put("Assignment status:", assignmentToFile.getStatus().getLabel());
		dataToFile.put("Assignment description:", assignmentToFile.getDescription());
		dataToFile.put("Assignment value:",
				assignmentToFile.getValueInStoryPoints() + " " + "Story Points");
		dataToFile.put("Assignment created:", assignmentToFile.getCreationDate().toString());
		dataToFile.put("Assignment is in project:", assignmentToFile.getProject().getName());
		if (assignmentToFile.getSprint() != null) {
			dataToFile.put("Assignment assigned to sprint:", assignmentToFile.getSprint().getName());
		} else {
			dataToFile.put("Assignment assigned to sprint:", " ");
		}
		if (assignmentToFile.getAssignee() != null) {
			dataToFile.put("Assignment assigned to team member:",
					assignmentToFile.getAssignee().getFullName());
		} else {
			dataToFile.put("Assignment assigned to team member:", " ");
		}
		dataToFile.put("Assignment last edited:", assignmentToFile.getEditedDate().toString());
		logger.debug("Assignment's data put into a map for csv file");
		return dataToFile.entrySet();
	}

	/**
	 * Used for the page view, when one of the assignment is selected from the
	 * assignments' table
	 * 
	 * @param event
	 */
	public void selectAssignment(SelectEvent event) {

		Assignment assignment = (Assignment) event.getObject();

		assignmentModel.setCurrentAssignment(assignment);

		projectModel.setManagedPro(assignment.getProject());

		ConfigurableNavigationHandler configurableNavigationHandler = (ConfigurableNavigationHandler) FacesContext
				.getCurrentInstance().getApplication().getNavigationHandler();

		configurableNavigationHandler.performNavigation("assignmentView");
	}

	/**
	 * Checks if the input assignment title is unique or already exist in the
	 * database.
	 * 
	 * @param context
	 * @param component
	 * @param value
	 * @throws ValidatorException
	 *             if the input title is found in the database, so the
	 *             assignment title is not unique.
	 */
	public void validateUniqueAssignmentNameInProject(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {

		String data = value.toString();
		Long projectId = projectModel.getManagedPro().getId();
		Assignment assignment = (Assignment) assignmentService.findAssignmentByNameAndProjectId(data, projectId);
		if (assignment == null || assignment.getId() == assignmentModel.getCurrentAssignment().getId()) {
			return;
		} else {
			FacesMessage message = new FacesMessage("Assignment must have a unique name in a project");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

	public void checkIfNewAssigneeIsNotDisabled(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		Member assignee = null;

		if (value != null) {
			assignee = memberService.findMemberById((Long) value);
		} else {
			return;
		}

		if (assignee != null && assignee.getStatus().equals(MemberStatus.DISABLED)) {
			FacesMessage message = new FacesMessage("Chosen user is disabled. Choose another assignee");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

	public AssignmentStatus[] getAssignmentStatuses() {
		return AssignmentStatus.values();
	}

	public SprintModel getSprintModel() {
		return sprintModel;
	}

	public void setSprintModel(SprintModel sprintModel) {
		this.sprintModel = sprintModel;
	}

	public AssignmentSearchCriterias getAssCriteria() {
		return assCriteria;
	}

	public void setAssCriteria(AssignmentSearchCriterias assCriteria) {
		this.assCriteria = assCriteria;
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public AssignmentModel getAssignmentModel() {
		return assignmentModel;
	}

	public void setAssignmentModel(AssignmentModel assignmentModel) {
		this.assignmentModel = assignmentModel;
	}

	public MemberModel getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberModel memberModel) {
		this.memberModel = memberModel;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public AssignmentService getAssignmentService() {
		return assignmentService;
	}

	public void setAssignmentService(AssignmentService assignmentService) {
		this.assignmentService = assignmentService;
	}

	public SprintService getSprintService() {
		return sprintService;
	}

	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}
}
