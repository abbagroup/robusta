package infobalt.robusta.ui.controllers;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Member.MemberStatus;
import infobalt.robusta.model.MemberModel;
import infobalt.robusta.model.RequestModel;
import infobalt.robusta.service.MemberService;

public class MemberController {

	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);

	private MemberModel model;

	private MemberService memberService;

	private RequestModel requestModel;

	private PasswordEncoder passwordEncoder;


	public void create() {
		model.setMember(new Member());
	}

	public void update(Member member) {
		model.setMember(member);
	}

	public void changePassword(Member member) {
		model.setMember(member);
	}

	public List<Member> loadMembers() {
		return memberService.findAllMembers();
	}

	public void cancel() {
		model.setMember(null);
	}

	public MemberStatus[] getMemberStatuses() {
		return MemberStatus.values();
	}

	/**
	 * Saves or updates certain user with all set parameters, such as name,
	 * password and other input data
	 */
	public void save() {
		Member member = model.getMember();

		if (!requestModel.getNewPassword().isEmpty()) {
			logger.debug("Password list is not empty");

			member.setPassword(requestModel.getNewPassword());
			logger.debug("User(ID{}) was set with password from the list", member.getId());

			member.setPassword(passwordEncoder.encode(member.getPassword()));
			logger.debug("User's(ID{}) password encoded", member.getId());
		}

		memberService.createMember(member);
		cancel();
		RequestContext rc = RequestContext.getCurrentInstance();
		rc.execute("PF('createMember').hide()");
	}

	public void delete(Member member) {
		memberService.deleteMember(member);
	}

	/**
	 * Set status DISABLED to a certain user, so that user can not login to the
	 * system
	 * 
	 * @param member
	 */
	public void disable(Member member) {
		member.setStatus(MemberStatus.DISABLED);
		logger.debug("User(ID{}) was disabled", member.getId());
		memberService.createMember(member);
		cancel();
	}
	
	/**
	 * Set status ACTIVE to a certain user, so that user can not login to the
	 * system
	 * 
	 * @param member
	 */
	public void activate(Member member) {
		member.setStatus(MemberStatus.ACTIVE);
		logger.debug("User(ID{}) was activated", member.getId());
		memberService.createMember(member);
		cancel();
	}


	/**
	 * Since email is used as a login name, it has to be unique. Method checks
	 * if the input email is unique or already exist in the database.
	 * 
	 * @param context
	 * @param component
	 * @param value
	 * @throws ValidatorException
	 *             if the input email is found in the database, so it is not
	 *             unique.
	 */
	public void validateUniqueEmail(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {

		String data = value.toString();

		for (Member member : memberService.findAllMembers()) {
			if (member.getLoginName().equalsIgnoreCase(data)) {
				logger.debug("User(ID{}) email is not ignored", member.getId());
				if (!(model.getMember().getId() == 0) || (model.getMember().getId() == member.getId())) {
					logger.debug("User(ID{}) email is not zero and is unique", member.getId());
					return;
				} else {
					FacesMessage message = new FacesMessage("A user with this email already exists");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					logger.error("User(ID{}) email is not unique", member.getId());
					throw new ValidatorException(message);
				}
			}
		}
	}

	/**
	 * Since password is a mandatory field it can't be empty. Method checks if
	 * input field is filed with a value.
	 * 
	 * @param context
	 * @param component
	 * @param value
	 * @throws ValidatorException
	 *             if input field is empty and password is not entered
	 */
	public void validateNewPassword(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {

		if (value.toString().isEmpty() && (model.getMember().getId() == null)) {
			FacesMessage message = new FacesMessage("A new user must have a password");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			logger.error("Password for user(ID{}) is not entered", model.getMember().getId());
			throw new ValidatorException(message);
		}
	}
	
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public RequestModel getProjectController2() {
		return requestModel;
	}

	public void setProjectController2(RequestModel requestModel) {
		this.requestModel = requestModel;
	}

	public MemberModel getModel() {
		return model;
	}

	public void setModel(MemberModel model) {
		this.model = model;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

}
