package infobalt.robusta.ui.controllers;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.entities.Sprint;
import infobalt.robusta.model.ProjectModel;
import infobalt.robusta.service.SprintService;

public class ScheduleController implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(ScheduleController.class);

	private static final long serialVersionUID = 81825048004213161L;

	private ScheduleModel eventModel;

	private SprintService sprintService;

	private ProjectModel projectModel;

	private ScheduleEvent event = new DefaultScheduleEvent();

	/**
	 * Initiate the new calendar schedule model when sprint is created and set
	 * with dates
	 */
	public void init() {
		setEventModel(new DefaultScheduleModel());
		List<Sprint> sprintList = sprintService.findByProject(projectModel.getManagedPro());
		logger.debug("Searched for project's sprints");

		for (Sprint sprint : sprintList) {
			Boolean isOverdue = sprintOverdue(sprint);
			ScheduleEvent sprintEvent = new DefaultScheduleEvent(sprint.getName(), sprint.getStartDate(),
					sprint.getEndDate(), (isOverdue ? "redEvent" : "blueEvent"));
			logger.debug("Checked if sprint is overdue and assigned the colour");
			eventModel.addEvent(sprintEvent);
		}
	}

	/**
	 * Checks if all sprints assignments are done when it ends.
	 * 
	 * @param sprint
	 * @return yes or no
	 */
	public Boolean sprintOverdue(Sprint sprint) {
		Date now = Calendar.getInstance().getTime();
		if (sprint.getEndDate().after(now)) {
			return false;
		}

		for (Assignment assignment : sprint.getSprintAssignments()) {
			if (assignment.getStatus() != AssignmentStatus.DONE) {
				return true;
			}
		}

		return false;
	}

	public Calendar today() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);

		return calendar;
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	public void addEvent(ActionEvent actionEvent) {
		if (event.getId() == null) {
			eventModel.addEvent(event);
			logger.debug("New event is added");
		} else {
			eventModel.updateEvent(event);
			logger.debug("Event is updated");
		}

		event = new DefaultScheduleEvent();
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (ScheduleEvent) selectEvent.getObject();
	}

	public void onDateSelect(SelectEvent selectEvent) {
		event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
	}

	public SprintService getSprintService() {
		return sprintService;
	}

	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}
}
