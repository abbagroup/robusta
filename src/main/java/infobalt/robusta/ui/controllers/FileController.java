package infobalt.robusta.ui.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.File;
import infobalt.robusta.model.AssignmentModel;
import infobalt.robusta.model.FileModel;
import infobalt.robusta.service.FileService;

public class FileController {

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	private FileModel fileModel;

	private FileService fileService;

	private AssignmentModel assignmentModel;

	public void upload(FileUploadEvent event) {
		try {
			File file = new File();
			byte[] fileByteArray = IOUtils.toByteArray(event.getFile().getInputstream());
			file.setFileByte(fileByteArray);
			file.setFileSize(event.getFile().getSize());
			logger.debug("File's size was established and set");
			file.setAssignment(assignmentModel.getCurrentAssignment());
			logger.debug("File's assignment is set");
			file.setFileName(event.getFile().getFileName());
			logger.debug("File's name is set");
			fileService.saveFile(file);
		} catch (IOException e) {
			logger.error("File was not uploaded");
			e.printStackTrace();
		}
	}

	public String fileSizeConverted(File file) {
		return fileService.fileSizeConverted(file);
	}

	public List<File> loadFilesByAssignment() {
		return fileService.findFilesByAssignment(assignmentModel.getCurrentAssignment());
	}

	public void download(File file) {
		InputStream stream = new ByteArrayInputStream(file.getFileByte());
		fileModel.setDownloadable(new DefaultStreamedContent(stream, null, file.getFileName()));
		logger.debug("File(ID{}) is downloaded", file.getId());
	}

	public FileModel getFileModel() {
		return fileModel;
	}

	public void setFileModel(FileModel fileModel) {
		this.fileModel = fileModel;
	}

	public FileService getFileService() {
		return fileService;
	}

	public void setFileService(FileService fileService) {
		this.fileService = fileService;
	}

	public AssignmentModel getAssignmentModel() {
		return assignmentModel;
	}

	public void setAssignmentModel(AssignmentModel assignmentModel) {
		this.assignmentModel = assignmentModel;
	}
}