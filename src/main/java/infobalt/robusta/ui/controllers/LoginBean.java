package infobalt.robusta.ui.controllers;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import infobalt.robusta.model.MemberModel;
import infobalt.robusta.model.ProjectModel;
import infobalt.robusta.service.MemberService;
import infobalt.robusta.service.ProjectService;

public class LoginBean {

	private static final Logger logger = LoggerFactory.getLogger(LoginBean.class);

	private String userName = null;
	private String password = null;

	private AuthenticationManager authenticationManager = null;

	private MemberModel memberModel;

	private MemberService memberService;

	private ProjectModel projectModel;

	private ProjectService projectService;

	public String login() {
		Authentication request;
		Authentication result = null;
		try {
			String loggedUserName = this.getUserName();
			String loggedUserPassword = this.getPassword();
			request = new UsernamePasswordAuthenticationToken(loggedUserName, loggedUserPassword);
			result = authenticationManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
			if (result != null) {
				logger.debug("Login and password passed the authentication");
				memberModel.setLoggedMember(memberService.findMemberByLogin(loggedUserName));
				logger.info("User(ID{}) logged in", memberModel.getLoggedMember().getId());
			}
		} catch (AuthenticationException e) {
			if (result == null) {
				if (FacesContext.getCurrentInstance().getMessageList().isEmpty()) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Email or password is incorrect"));
					logger.error("Entered wrong login or password");
				}
				return null;
			}
			return "incorrect";
		}
		
		if (((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getAuthorities().iterator()
				.next().toString().equalsIgnoreCase("ROLE_USER")) {
			logger.debug("User logged in is regular (not admin)");
			return "Home";
		}
		return "correct";
	}

	public String cancel() {
		setUserName(null);
		setPassword(null);
		return "login";
	}

	public String logout() {
		SecurityContextHolder.clearContext();
		logger.debug("User(ID{}) logged out", memberModel.getLoggedMember().getId());
		memberModel.setLoggedMember(null);
		return "loggedout";
	}

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public MemberModel getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberModel memberModel) {
		this.memberModel = memberModel;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

}