package infobalt.robusta.ui.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Assignment.AssignmentStatus;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Project;
import infobalt.robusta.entities.Sprint;
import infobalt.robusta.model.MemberModel;
import infobalt.robusta.model.ProjectModel;
import infobalt.robusta.model.SprintModel;
import infobalt.robusta.service.AssignmentService;
import infobalt.robusta.service.SprintService;

public class SprintController implements Serializable {

	private static final long serialVersionUID = -1389684125871062390L;

	static final Logger logger = LoggerFactory.getLogger(SprintController.class);

	private SprintModel sprintModel;

	private SprintService sprintService;

	private ProjectModel projectModel;

	private AssignmentService assignmentService;

	private MemberModel memberModel;

	public void createSprint() {
		sprintModel.setEditedSprint(new Sprint());
		sprintModel.getEditedSprint().setProject(projectModel.getManagedPro());
	}

	public void updateSprint(Sprint sprint) {
		sprintModel.setEditedSprint(sprint);
		sprintService.updateAndSaveBurndownHistory(sprint);
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public List<Sprint> loadSprints() {
		return sprintService.findAllSprints();
	}

	public void cancelSprint() {
		sprintModel.setEditedSprint(null);
	}

	/**
	 * Saves or updates the sprint setting input values to it such as dates,
	 * chosen assignments if any, and other *
	 */
	public void saveSprint() {
		List<Long> assignmentsIds = sprintModel.getAssignmentsId();
		sprintModel.getEditedSprint().getSprintAssignments().clear();

		List<Assignment> assignments = assignmentService.getAssignmentRepo().findAllByIds(assignmentsIds);

		for (Assignment assignment : assignments) {

			sprintModel.getEditedSprint().getSprintAssignments().add(assignment);
			assignment.setSprint(sprintModel.getEditedSprint());
			logger.debug("Sprint's assignments are set");

		}
		adjustEndDate();
		sprintService.saveSprint(sprintModel.getEditedSprint());
		cancelSprint();
	}

	public void deleteSprint(Sprint sprint) {

		sprintService.removeAssignments(sprint);
		sprintService.removeBurndownData(sprint);

		sprintService.deleteSprint(sprint);
		sprintModel.setSprint(null);
	}

	/**
	 * By activating sprint, the burn-down chart data starts to save and the
	 * line model out of them is started to be created
	 * 
	 * @return sprint view page with updated burn-down chart
	 */
	public String activateSprint() {
		Sprint sprint = sprintModel.getSprint();
		Project project = projectModel.getManagedPro();
		sprintService.activateSprint(sprint, project);
		logger.debug("Sprint(ID{}) changed to ACTIVE", sprint.getId());
		sprintService.updateAndSaveBurndownHistory(sprint);

		return "sprintView";
	}

	public Boolean canActivateSprint() {
		if (memberModel.getLoggedMember().getLoginName()
				.equals(projectModel.getManagedPro().getProjectManager().getLoginName())) {

			Sprint sprint = sprintModel.getSprint();
			return sprintService.canActivateSprint(sprint);
		}
		return false;
	}

	public Boolean canCompleteSprint() {
		if (memberModel.getLoggedMember().getLoginName()
				.equals(projectModel.getManagedPro().getProjectManager().getLoginName())) {
			Sprint sprint = sprintModel.getSprint();
			return sprintService.canCompleteSprint(sprint);
		}
		return false;
	}

	/**
	 * By completing the sprint burn-down chart data put the last record and the
	 * line model is stopped and can not be modified after (when sprint status
	 * is "completed"
	 * 
	 * @return sprint view page with updated burn-down chart
	 */
	public String completeSprint() {
		Sprint sprint = sprintModel.getSprint();
		sprintService.completeSprint(sprint);
		logger.debug("Sprint(ID{}) changed to COMPLETED", sprint.getId());
		sprintService.updateAndSaveBurndownHistory(sprint);

		return "sprintView";

	}

	public List<Sprint> loadProjectSprints() {
		Project managedProject = projectModel.getManagedPro();
		return sprintService.findByProject(managedProject);
	}

	public List<Sprint> loadCurrentSprint() {
		Project managedProject = projectModel.getManagedPro();
		return sprintService.findByCurrentSprint(managedProject);
	}

	public Project loadCurrentProject() {
		return projectModel.getManagedPro();
	}

	/**
	 * save the record of sprint's end for calendar schedule, so that the end
	 * wouldn't be on 0:00 of the last day, but 20 hours after
	 */
	public void adjustEndDate() {
		sprintModel.getEditedSprint().getEndDate().setHours(20);
		logger.debug("Sprint's end date was adjusted");
	}

	public Date getNowDate() {

		Calendar cal = Calendar.getInstance();
		Date tomorrow = cal.getTime();
		return tomorrow;
	}

	public List<Long> loadAssignmentsIds() {
		Project managedPro = projectModel.getManagedPro();
		return assignmentService.findAssignmentsIdListByProject(managedPro);
	}

	public List<Long> getAssignmentsId() {
		if (sprintModel.getAssignmentsId() == null) {
			Sprint assignmentSprint = sprintModel.getSprint();
			if (assignmentSprint.getId() == null || assignmentSprint.getId() == 0) {
				sprintModel.setAssignmentsId(new ArrayList<Long>());
			} else {
				sprintModel.setAssignmentsId(assignmentService.findAssignmentsIdListBySprint(assignmentSprint));
			}
		}
		return sprintModel.getAssignmentsId();
	}

	public void setAssignmentsId(List<Long> assignmentsId) {
		sprintModel.setAssignmentsId(assignmentsId);
	}

	public void submitAssignmentToSprint() {
		List<Long> assignmentsIds = sprintModel.getAssignmentsId();
		sprintModel.getEditedSprint().getSprintAssignments().clear();

		for (Long assignmentID : assignmentsIds) {

			Assignment submitedAssignment = assignmentService.getAssignmentRepo().findById(assignmentID);
			sprintModel.getSprint().getSprintAssignments().add(submitedAssignment);
			logger.debug("Assignment(ID{}) is assigned to sprint", assignmentID);
		}
		sprintService.saveSprint(sprintModel.getEditedSprint());
		sprintService.updateAndSaveBurndownHistory(sprintModel.getEditedSprint());
	}

	/**
	 * Used for the view of a certain sprint to show all it's data on the sprint
	 * view page
	 */
	public void openSprint(Sprint sprint) {
		Sprint sprintToShow = sprintService.loadSprint(sprint);
		sprintModel.setSprint(sprintToShow);
	}

	public List<Assignment> loadAssignmentsBySprint() {
		return sprintModel.getSprint().getSprintAssignments();
	}

	public int countValueInStoryPoints() {
		return sprintService.countStoryPointsPerSprint(sprintModel.getSprint());
	}

	public int countDoneValueInStoryPoints() {
		return sprintService.countDoneStoryPointsPerSprint(sprintModel.getSprint());
	}

	public int countNotDoneValueInStoryPoints() {
		return sprintService.countNotDoneStoryPointsPerSprint(sprintModel.getSprint());
	}

	/**
	 * This and all others "load10" methods used for rational view, when only 10
	 * latest records are shown on the page
	 */
	public List<Assignment> load10ToDoAssignments() {
		return sprintService.loadAssignmentsByStatus(sprintModel.getSprint(), AssignmentStatus.TODO, true);
	}

	public List<Assignment> load10InProgressAssignments() {
		return sprintService.loadAssignmentsByStatus(sprintModel.getSprint(), AssignmentStatus.INPROGRESS, true);
	}

	public List<Assignment> load10DoneAssignments() {
		return sprintService.loadAssignmentsByStatus(sprintModel.getSprint(), AssignmentStatus.DONE, true);
	}

	public List<Assignment> loadNotInSprintAssignments() {
		return sprintService.loadAssignmentsNotInSprint(projectModel.getManagedPro());
	}

	/**
	 * The total amount of certain sprint's days is needed for burn-down chart
	 * xAxis
	 * 
	 * @return integer of full days(starting 1) between sprint's start and end
	 *         dates
	 */
	public int countCurrentSprintDays() {
		return sprintService.countSprintDays(sprintModel.getSprint());
	}

	/**
	 * Creates X and Y axis for burn-down chart line, where X axis is total
	 * amount of sprint days (integer) and Y axis is total amount (integer) of
	 * story points left to do
	 * 
	 * @return chart model with set X and Y axis and their values
	 */
	public LineChartModel createSprintLineModel() {

		LineChartModel sprintLine = new LineChartModel();
		sprintLine = initCategoryModel();
		sprintLine.setTitle("Sprint" + " '" + sprintModel.getSprint().getName() + "' " + "BurnDownChart");
		sprintLine.setLegendPosition("e");
		sprintLine.setShowPointLabels(true);
		Axis yAxis = sprintLine.getAxis(AxisType.Y);
		Axis xAxis = sprintLine.getAxis(AxisType.X);
		xAxis.setTickInterval("1");
		xAxis.setLabel("Sprint Days");
		xAxis.setMin(Integer.valueOf(1));

		if (countCurrentSprintDays() < sprintModel.getSprint().getBurndowData().get(0).getSprintPeriodInDays()) {
			xAxis.setMax(sprintModel.getSprint().getBurndowData().get(0).getSprintPeriodInDays());
			logger.debug("Sprint's X axis max value is set from data table");
		} else {
			xAxis.setMax(Integer.valueOf(countCurrentSprintDays()));
			logger.debug("Sprint's X axis max value is set from last sprint day count");
		}

		yAxis.setLabel("Story Points Left To Do");
		yAxis.setMin(Integer.valueOf(0));

		if (countValueInStoryPoints() < sprintModel.getSprint().getBurndowData().get(0)
				.getTotalStoryPointsPerSprint()) {
			yAxis.setMax(sprintModel.getSprint().getBurndowData().get(0).getTotalStoryPointsPerSprint());
			logger.debug("Sprint's Y axis max value is set from data table");
		} else {
			yAxis.setMax(Integer.valueOf(countValueInStoryPoints()));
			logger.debug("Sprint's Y axis max value is set from last story points count");
		}
		yAxis.setTickInterval("5");
		return sprintLine;
	}

	/**
	 * Creates sprint burn-down data line, depending on saved records (which day
	 * how many story points left to do).
	 * 
	 * @return model of two lines - one is expected progress where all story
	 *         points left to do on the first day and 0 story points left to do
	 *         on the last day of sprint; another line shows the actual progress
	 *         according to the saved burn-down chart data
	 */
	private LineChartModel initCategoryModel() {
		Map<Object, Number> expectedSprintData = sprintService.getExpectedBurndownData(sprintModel.getSprint());
		Map<Object, Number> actualSprintData = sprintService.getSprintBurndownData(sprintModel.getSprint());
		LineChartModel model = new LineChartModel();

		ChartSeries expected = new ChartSeries();
		expected.setLabel("Expected progress");
		expected.setData(expectedSprintData);

		ChartSeries actual = new ChartSeries();
		actual.setLabel("Actual progress");
		actual.setData(actualSprintData);

		model.addSeries(actual);
		logger.debug("Sprint's actual data added to the model");
		model.addSeries(expected);
		logger.debug("Sprint's expected data added to the model");

		return model;
	}

	/**
	 * Used for the page view, when one of the sprint is selected from the
	 * sprints' table
	 * 
	 * @param event
	 */
	public void selectSprint(SelectEvent event) {

		Sprint sprint = (Sprint) event.getObject();
		sprintModel.setSprint(sprint);

		ConfigurableNavigationHandler configurableNavigationHandler = (ConfigurableNavigationHandler) FacesContext
				.getCurrentInstance().getApplication().getNavigationHandler();

		configurableNavigationHandler.performNavigation("sprintView");
	}

	public List<Assignment> loadUnassignedAssignmentsInSprint() {
		return sprintService.loadUnassignedAssignmentsInSprint(sprintModel.getSprint());
	}

	public boolean areThereAnyUnassignedAssignmentsInThisSprint() {
		boolean i = (sprintService.loadUnassignedAssignmentsInSprint(sprintModel.getSprint()).size() > 0);
		if (i == true) {
			logger.info("There are unassigned assignments in sprint");
		}
		return i;
	}

	public List<Assignment> loadAssignmentsByStatusInSprint() {
		return sprintService.loadAssignmentsByStatus(sprintModel.getSprint(), sprintModel.getWatchedAssignmentStatus(),
				false);
	}

	public void setWatchedAssignmentsStatusesToToDo() {
		sprintModel.setWatchedAssignmentStatus(AssignmentStatus.TODO);
	}

	public void setWatchedAssignmentsStatusesToInProgress() {
		sprintModel.setWatchedAssignmentStatus(AssignmentStatus.INPROGRESS);
	}

	public void setWatchedAssignmentsStatusesToDone() {
		sprintModel.setWatchedAssignmentStatus(AssignmentStatus.DONE);
	}

	public boolean isRenderedIfCurrentSprintExists() {
		return !sprintService.findByCurrentSprint(projectModel.getManagedPro()).isEmpty();
	}

	public MemberModel getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberModel memberModel) {
		this.memberModel = memberModel;
	}

	public SprintModel getSprintModel() {
		return sprintModel;
	}

	public void setSprintModel(SprintModel sprintModel) {
		this.sprintModel = sprintModel;
	}

	public SprintService getSprintService() {
		return sprintService;
	}

	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}

	public AssignmentService getAssignmentService() {
		return assignmentService;
	}

	public void setAssignmentService(AssignmentService assignmentService) {
		this.assignmentService = assignmentService;
	}

}
