package infobalt.robusta.ui.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Member.MemberStatus;
import infobalt.robusta.entities.Project;
import infobalt.robusta.model.AssignmentModel;
import infobalt.robusta.model.MemberModel;
import infobalt.robusta.model.ProjectModel;
import infobalt.robusta.model.RequestModel;
import infobalt.robusta.service.MemberService;
import infobalt.robusta.service.ProjectService;

public class ProjectController {

	static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

	private ProjectModel projectModel;

	private ProjectService projectService;

	private MemberService memberService;

	private MemberModel memberModel;

	private RequestModel requestModel;

	private AssignmentModel assignmentModel;

	public void createProject() {
		projectModel.setProject(new Project());
		projectModel.setManagerId(null);
	}

	public void updateProject(Project project) {
		projectModel.setProject(project);
		if (project.getProjectManager() != null) {
			projectModel.setManagerId(project.getProjectManager().getId());
		}
	}

	public List<Project> loadProjects() {
		return projectService.findAllProjects();

	}

	public void cancelProject() {
		projectModel.setProject(null);
	}

	/**
	 * Saves or updates certain project with all set parameters, such as project
	 * manager, assignees, and other input data
	 */
	public void saveProject() {
		Long managerId = projectModel.getManagerId();
		if (managerId != null) {
			Member selectedMember = memberService.getMemberDAO().findById(managerId);
			projectModel.getProject().setProjectManager(selectedMember);
			logger.debug("Project manager is set");
			List<Member> workers = new ArrayList<>();
			workers.add(selectedMember);
			projectModel.getProject().setWorkers(workers);
			logger.debug("Project manager is set as a users");
		}
		projectService.createProject(projectModel.getProject());
		cancelProject();
		RequestContext rc = RequestContext.getCurrentInstance();
		rc.execute("PF('createProject').hide()");
	}

	public void deleteProject(Project project) {
		projectService.deleteProject(project);
	}

	public List<Member> getMembers() {
		return memberService.findAllMembers();
	}

	public List<Member> getMembersAvailableForProject() {
		return memberService.findAllMembers();
	}

	public List<Project> loadManagedProjects() {
		Member loggedManager = memberModel.getLoggedMember();
		return projectService.findProjectsByManager(loggedManager);
	}

	public void manageProject(Project project) {
		Project managedProject = projectService.manageProject(project);
		projectModel.setManagedPro(managedProject);
	}

	public List<Long> loadWorkersIds() {
		Project managedPro = projectModel.getManagedPro();
		return memberService.findWorkersIdListByProject(managedPro);
	}

	public List<Long> getWorkersIds() {
		if (projectModel.getWorkersIds() == null) {
			Project managedPro = projectModel.getManagedPro();
			projectModel.setWorkersIds(memberService.findWorkersIdListByProject(managedPro));
		}
		return projectModel.getWorkersIds();
	}

	public void setWorkersIds(List<Long> workersIds) {
		projectModel.setWorkersIds(workersIds);
	}

	public void assignWorkers() {
		List<Long> workersIDs = requestModel.getWorkersIds();
		projectModel.getManagedPro().getWorkers().clear();
		logger.debug("Manager project's workers list was cleared");

		for (Long workerID : workersIDs) {

			if (memberService.getMemberDAO().findById(workerID).getStatus() == MemberStatus.DISABLED) {
				FacesMessage message = new FacesMessage(
						"This user's " + memberService.getMemberDAO().findById(workerID).getFullName()
								+ " account is disabled so he can not be assigned to any project", null);
				FacesContext.getCurrentInstance().addMessage(null, message);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				logger.error("Tried to assign disabled user");
				continue;
			}

			Member assignWorker = memberService.getMemberDAO().findById(workerID);
			projectModel.getManagedPro().getWorkers().add(assignWorker);
			logger.debug("User(ID{}) is assigned to manager's project", workerID);
		}
		projectService.createProject(projectModel.getManagedPro());
	}

	/**
	 * Loads all projects which user is assigned to
	 */
	public List<Project> loadWorkingProjects() {
		Member loggedWorker = memberModel.getLoggedMember();
		return projectService.findProjectsByWorker(loggedWorker);
	}

	/**
	 * Counts total amount of story points per project
	 * 
	 * @return integer of all story points
	 */
	public int countTotalValue() {
		return projectService.countStoryPointsPerProject(projectModel.getManagedPro());
	}

	/**
	 * Counts total amount of story points of assignments with status DONE
	 * 
	 * @return integer of all DONE assignments' story points
	 */
	public int countDoneValue() {
		return projectService.countDoneStoryPointsPerProject(projectModel.getManagedPro());
	}

	/**
	 * Counts total amount of story points of assignments with status TODO and
	 * INPROGRESS
	 * 
	 * @return integer of all TODO and INPROGRESS assignments' story points
	 */
	public int countNotDoneValue() {
		return projectService.countNotDoneStoryPointsPerProject(projectModel.getManagedPro());
	}

	/**
	 * Counts the percentage of DONE assignments story points relatively to
	 * total amount of story points in project
	 * 
	 * @return integer value of percent
	 */
	public int countPercentage() {
		return projectService.countPercentageOfDonePerProject(projectModel.getManagedPro());
	}

	/**
	 * According to DONE assignments' relativity to project period, counts how
	 * many assignments on average were changed to DONE per day in a certain
	 * project.
	 * 
	 * @return double type value of assignments set as DONE per project day
	 */
	public double countAssignmentsPerDay() {
		return projectService.countAssignmentsPerDayPerProject(projectModel.getManagedPro());
	}

	/**
	 * According to total value of DONE assignments' story points relativity to
	 * project period, counts how many story points on average were DONE per day
	 * in a certain project.
	 * 
	 * @return double type value of story points DONE per project day
	 */
	public double countStoryPointPerDay() {
		return projectService.countStoryPointsPerDayPerProject(projectModel.getManagedPro());
	}

	/**
	 * Used for the page view, when one of the project is selected from the
	 * projects' table
	 * 
	 * @param event
	 */
	public void selectProject(SelectEvent event) {

		Project project = (Project) event.getObject();
		projectModel.setManagedPro(project);
		assignmentModel.setFoundAssignments(null);
		ConfigurableNavigationHandler configurableNavigationHandler = (ConfigurableNavigationHandler) FacesContext
				.getCurrentInstance().getApplication().getNavigationHandler();
		configurableNavigationHandler.performNavigation("manageProject");
	}

	/**
	 * Method tells if a certain attribute in a page view can be seen only by
	 * project manager (and not to all the users)
	 * 
	 * @return yes or no
	 */
	public boolean seenToProjectManager() {
		if (memberModel.getLoggedMember().getLoginName()
				.equals(projectModel.getManagedPro().getProjectManager().getLoginName())) {
			return true;
		}
		return false;
	}

	public int countAssignments() {
		return projectService.countAssignment(projectModel.getManagedPro());
	}

	public int countDoneAssignments() {
		return projectService.countDoneAssignment(projectModel.getManagedPro());
	}

	public int countNotDoneAssignments() {
		return projectService.countAssignmentsNotDone(projectModel.getManagedPro());
	}

	/**
	 * Method tells if a certain user is assigned as a project manager to any
	 * project
	 * 
	 * @return yes or no
	 */
	public boolean hasMemberAnyManagedProjects() {
		if (projectService.countManagedProjectsForASpecificMember(memberModel.getLoggedMember()) > 0) {
			return true;
		}

		return false;
	}

	/**
	 * Method tells if a certain user is assigned as an assignee to any project
	 * 
	 * @return yes or no
	 */
	public boolean hasMemberAnyWorkingProjects() {
		if (projectService.countWorkingProjectsForASpecificMember(memberModel.getLoggedMember()) > 0) {
			return true;
		}

		return false;
	}

	/**
	 * Checks if the input project title is unique or already exist in the
	 * database.
	 * 
	 * @param context
	 * @param component
	 * @param value
	 * @throws ValidatorException
	 *             if the input title is found in the database, so the title is
	 *             not unique.
	 */
	public void validateUniqueProjectName(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {

		String data = value.toString();

		for (Project project : projectService.findAllProjects()) {
			if (project.getName().equalsIgnoreCase(data)) {
				if ((projectModel.getProject().getId() != null)
						|| projectModel.getProject().getId() == project.getId()) {
					return;
				} else {
					FacesMessage message = new FacesMessage("Project must have a unique name");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					logger.error("Entered project's title was not unique");
					throw new ValidatorException(message);
				}
			}
		}
	}

	public AssignmentModel getAssignmentModel() {
		return assignmentModel;
	}

	public void setAssignmentModel(AssignmentModel assignmentModel) {
		this.assignmentModel = assignmentModel;
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public MemberModel getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberModel memberModel) {
		this.memberModel = memberModel;
	}

	public RequestModel getProjectController2() {
		return requestModel;
	}

	public void setProjectController2(RequestModel requestModel) {
		this.requestModel = requestModel;
	}

}
