package infobalt.robusta.ui.controllers;

import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Comment;
import infobalt.robusta.model.AssignmentModel;
import infobalt.robusta.model.CommentModel;
import infobalt.robusta.model.MemberModel;
import infobalt.robusta.service.CommentService;

public class CommentController {

	private static final Logger logger = LoggerFactory.getLogger(CommentController.class);

	private CommentModel model;

	private CommentService commentService;

	private MemberModel memberModel;

	private AssignmentModel assignmentModel;

	public CommentController() {
	}

	public List<Comment> loadComments() {
		return commentService.findAllComments();
	}

	public void delete(Comment comment) {
		if (canDelete(comment)) {
			comment.setAssignment(null);
			comment.setAuthor(null);
			commentService.deleteComment(comment);

			FacesMessage message = new FacesMessage("Comment deleted");
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			logger.error("Comment deleted");

		} else {

			FacesMessage message = new FacesMessage("It is too late to delete a comment, please contact admin");
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			logger.error("Comment can not be deleted the time is out");

		}
	}

	public CommentModel getModel() {
		return model;
	}

	public void setModel(CommentModel model) {
		this.model = model;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void submitComment() {
		Comment comment = new Comment();
		comment.setCommentText(model.getCommentText());
		comment.setAuthor(memberModel.getLoggedMember());
		logger.debug("Comment is set to its author(user)");
		comment.setAssignment(assignmentModel.getCurrentAssignment());
		logger.debug("Comment is set to its assignment");
		if (comment.getCommentText().isEmpty()) {
			FacesMessage message = new FacesMessage("Comment must have at least one symbol");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			return;
		} else {
			commentService.createComment(comment);
			model.setCommentText(null);
			FacesMessage message = new FacesMessage("Comment added and it can be deleted within one minute.");
			message.setSeverity(FacesMessage.SEVERITY_INFO);
		}
		model.getCommentText();
	}

	public List<Comment> loadCommentsByAssignment() {
		if (!(assignmentModel.getCurrentAssignment().getId() == null)) {
			return commentService.loadCommentsByAssignment(assignmentModel.getCurrentAssignment());
		}
		return null;
	}

	public boolean canDelete(Comment comment) {
		if (memberModel.getLoggedMember().getRole().equalsIgnoreCase("admin")) {
			logger.debug("Comment can be deleted by admin");
			return true;
		}

		if (memberModel.getLoggedMember().getId().equals(comment.getAuthor().getId())) {
			Calendar dateCreated = Calendar.getInstance();
			dateCreated.setTime(comment.getCommentDate());

			Calendar dateNow = Calendar.getInstance();
			dateNow.setTime(new java.util.Date(Calendar.getInstance().getTimeInMillis()));
			dateNow.add(Calendar.SECOND, -60);

			if (dateNow.compareTo(dateCreated) <= 0) {
				logger.debug("Comment can be deleted on time");
				return true;
			}
		}
		return false;
	}

	public AssignmentModel getAssignmentModel() {
		return assignmentModel;
	}

	public MemberModel getMemberModel() {
		return memberModel;
	}

	public void setAssignmentModel(AssignmentModel assignmentModel) {
		this.assignmentModel = assignmentModel;
	}

	public void setMemberModel(MemberModel memberModel) {
		this.memberModel = memberModel;
	}
}
