package infobalt.robusta.ui.lazyModels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import infobalt.robusta.entities.Comment;
import infobalt.robusta.service.CommentService;

public class LazyCommentsModel extends LazyDataModel<Comment> {

	private static final long serialVersionUID = -5084086056371592586L;

	private List<Comment> comments;

	private CommentService commentService;

	@Override
	public List<Comment> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		changeFilters(filters);

		try {
			comments = commentService.findLazyComments(first, pageSize, sortField, sortOrder, filters);
		} catch (Exception e) {
			e.printStackTrace();
		}

		setRowCount(commentService.countLazyComments(filters));

		return comments;
	}

	private Map<String, Object> changeFilters(Map<String, Object> filters) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;

		if (filters.containsKey("commentDate")) {
			String creationDate = (String) filters.get("commentDate");
			if (!creationDate.startsWith("-")) {
				try {
					date = (Date) formatter.parse(creationDate.substring(0, 10));
					filters.put("creationDateFrom", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			} else if (creationDate.length() > 2) {
				try {
					date = (Date) formatter.parse(creationDate.substring(1, 11));
					filters.put("creationDateTo", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			}

			if (creationDate.length() > 11) {
				try {
					date = (Date) formatter.parse(creationDate.substring(11));
					filters.put("creationDateTo", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			}
		}
		return filters;
	};

	@Override
	public Comment getRowData(String rowKey) {
		return commentService.findCommentById(Long.valueOf(rowKey));
	}

	@Override
	public Object getRowKey(Comment comment) {
		return comment.getId();
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

}
