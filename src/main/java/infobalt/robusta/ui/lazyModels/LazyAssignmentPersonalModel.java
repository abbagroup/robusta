package infobalt.robusta.ui.lazyModels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.model.MemberModel;
import infobalt.robusta.service.AssignmentService;

public class LazyAssignmentPersonalModel extends LazyDataModel<Assignment> {

	private static final long serialVersionUID = 1L;

	private List<Assignment> assignments;

	private AssignmentService assignmentService;

	private MemberModel memberModel;
	@Override
	public List<Assignment> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
	
		changeFilters(filters);
		
		try {

			assignments = assignmentService.findLazyAssignmentsByMember(memberModel.getLoggedMember(), first, pageSize, sortField, sortOrder, filters);

		} catch (Exception e) {
			e.printStackTrace();
		}

			setRowCount(assignmentService.countTotalAssignmentsByMember(memberModel.getLoggedMember(), filters));

		return assignments;
	}
	
	private Map<String, Object> changeFilters(Map<String, Object> filters){
		
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		
		if(filters.containsKey("creationDate")){
			String creationDate = (String) filters.get("creationDate");
			if(!creationDate.startsWith("-")){
				try {
					date = (Date) formatter.parse(creationDate.substring(0, 10));
					filters.put("creationDateFrom", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			}else if(creationDate.length() > 2){
				try {
					date = (Date) formatter.parse(creationDate.substring(1, 11));
					filters.put("creationDateTo", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			}
			
			if(creationDate.length()> 11){
				try {
					date = (Date) formatter.parse(creationDate.substring(11));
					filters.put("creationDateTo", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			}
		}
		
		if(filters.containsKey("editedDate")){
			String editedDate = (String) filters.get("editedDate");
			if(!editedDate.startsWith("-")){
				try {
					date = (Date) formatter.parse(editedDate.substring(0, 10));
					filters.put("editedDateFrom", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			}else if(editedDate.length() > 2){
				try {
					date = (Date) formatter.parse(editedDate.substring(1, 11));
					filters.put("editedDateTo", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			}
			
			if(editedDate.length()> 11){
				try {
					date = (Date) formatter.parse(editedDate.substring(11));
					filters.put("editedDateTo", date);
				} catch (ParseException e) {
					System.out.println("didn't work");
				}
			}
		}
		
		return filters;
	};
	
	
	@Override
	public Assignment getRowData(String rowKey) {
		return assignmentService.findAssingmentById(Long.valueOf(rowKey));
    }
	
	@Override
    public Object getRowKey(Assignment assignment) {
    	return assignment.getId();
    }


	public List<Assignment> getAssignments() {
		return assignments;
	}

	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}

	public AssignmentService getAssignmentService() {
		return assignmentService;
	}

	public void setAssignmentService(AssignmentService assignmentService) {
		this.assignmentService = assignmentService;
	}

	public MemberModel getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberModel memberModel) {
		this.memberModel = memberModel;
	}

	

	
}
