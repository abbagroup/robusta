package infobalt.robusta.supportClasses;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import infobalt.robusta.entities.Member;
import infobalt.robusta.repositoriesJPA.MemberDAO;

public class MemberConverter implements Converter {

	private MemberDAO memberDAO;
	
	public void setMemberDAO(MemberDAO memberDAO) {
		this.memberDAO = memberDAO;
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		  
            try {
                  
                Object obj =  memberDAO.findById(Long.valueOf(value));

                return obj;
            } catch (Exception e) {
                throw new ConverterException(new FacesMessage(String.format("Cannot convert %s to User", value)), e);
            }
		
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (!(value instanceof Member)) {
            return null;
        }

       String s =  String.valueOf(((Member) value).getId());
       
       return s;
	}

}
