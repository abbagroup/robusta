package infobalt.robusta.supportClasses;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PassEncode {
	
	public static void main(String[] args) {
		String pass = "abba";
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		String hashed = encoder.encode(pass);
		System.out.println(hashed);
	}

}
