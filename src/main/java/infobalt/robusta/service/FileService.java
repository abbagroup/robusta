package infobalt.robusta.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.File;
import infobalt.robusta.repositories.FileRepository;

public class FileService {

	private static final Logger logger = LoggerFactory.getLogger(FileService.class);

	private FileRepository fileRepository;

	public List<File> findAllFiles() {
		return fileRepository.findAllFiles();
	}

	public void saveFile(File file) {
		fileRepository.saveFile(file);
	}

	public void deleteFile(File file) {
		fileRepository.deleteFile(file);
	}

	public List<File> findFilesByAssignment(Assignment currentAssignment) {
		return fileRepository.findFilesByAssignment(currentAssignment);
	}

	public String fileSizeConverted(File file) {
		Long size = file.getFileSize();
		logger.debug("Checked file's actual size");
		String sizeString = null;
		BigDecimal sizeConverted = null;
		if ((size / (1024 * 1024)) > 0) {
			logger.debug("File's(ID{}) size devided on (1024*1024) is larger than zero", file.getId());
			sizeConverted = BigDecimal.valueOf(size).divide(new BigDecimal((new Integer(1024 * 1024)).toString()));
			sizeConverted = sizeConverted.setScale(2, RoundingMode.HALF_UP);
			sizeString = sizeConverted + " MB";
		} else if ((size / 1024) > 0) {
			logger.debug("File's(ID{}) size devided on 1024 is larger than zero", file.getId());
			sizeConverted = BigDecimal.valueOf(size).divide(new BigDecimal((new Integer(1024)).toString()));
			sizeConverted = sizeConverted.setScale(2, RoundingMode.HALF_UP);
			sizeString = sizeConverted + " KB";
		} else {
			logger.debug("File's(ID{}) size devided on 1024 is less than zero", file.getId());
			sizeString = size + " B";
		}
		return sizeString;
	}

	public FileRepository getFileRepository() {
		return fileRepository;
	}

	public void setFileRepository(FileRepository fileRepository) {
		this.fileRepository = fileRepository;
	}

}
