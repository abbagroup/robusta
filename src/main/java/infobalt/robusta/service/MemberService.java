package infobalt.robusta.service;

import java.util.Calendar;
import java.util.List;

import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;
import infobalt.robusta.repositories.MemberRepository;

public class MemberService {

	private MemberRepository memberDAO;

	public void createMember(Member member) {
		if (member.getId() == null) {
			java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
			member.setRegistrationDate(date);
		}
		memberDAO.create(member);
	}

	public List<Member> findAllMembers() {
		return memberDAO.findAllMembers();
	}

	public void deleteMember(Member member) {
		memberDAO.delete(member);
	}

	public Member findMemberByLoginAndPassword(String login, String password) {
		List<Member> foundMembers = memberDAO.findByLoginAndPassword(login, password);
		if (foundMembers != null && !foundMembers.isEmpty())
			return foundMembers.get(0);
		else
			return null;
	}

	public Member findMemberByLogin(String login) {
		Member foundMember = memberDAO.findByLogin(login);
		if (foundMember != null)
			return foundMember;
		else
			return null;
	}

	public List<Member> findWorkersByProject(Project project) {
		return memberDAO.findWorkersByProject(project);
	}

	public List<Long> findWorkersIdListByProject(Project project) {
		List<Long> workers = memberDAO.findWorkersIdsByProject(project);
		return workers;
	}

	public Member findMemberById(Long id) {
		if (id == 0) {
			return null;
		}
		return memberDAO.findById(id);
	}

	public MemberRepository getMemberDAO() {
		return memberDAO;
	}

	public void setMemberDAO(MemberRepository memberDAO) {
		this.memberDAO = memberDAO;
	}

}
