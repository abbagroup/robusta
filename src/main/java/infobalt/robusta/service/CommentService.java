package infobalt.robusta.service;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Comment;
import infobalt.robusta.repositories.CommentRepository;

public class CommentService {

	private CommentRepository commentDAO;

	public List<Comment> findAllComments() {
		return commentDAO.findAllComments();
	}

	public void deleteComment(Comment comment) {
		commentDAO.deleteComment(comment);
	}

	public void createComment(Comment comment) {
		java.util.Date date = new java.util.Date(Calendar.getInstance().getTimeInMillis());
		comment.setCommentDate(date);
		commentDAO.createComment(comment);
	}

	public List<Comment> loadCommentsByAssignment(Assignment currentAssignment) {
		return commentDAO.findCommentsByAssignment(currentAssignment);
	}

	public void setCommentDAO(CommentRepository commentDAO) {
		this.commentDAO = commentDAO;
	}

	public CommentRepository getCommentDAO() {
		return commentDAO;
	}

	public List<Comment> findLazyComments(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		return commentDAO.findAllLazyComments(first, pageSize, sortField, sortOrder,
				filters);
	}

	public int countLazyComments(Map<String, Object> filters) {
		return commentDAO.countTotalComments(filters);
	}

	public Comment findCommentById(Long id) {
		return commentDAO.findCommentById(id);
	}
}
