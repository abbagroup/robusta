package infobalt.robusta.service;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;
import infobalt.robusta.repositories.AssignmentRepository;
import infobalt.robusta.repositories.ProjectRepository;

public class ProjectService {

	private static final Logger logger = LoggerFactory.getLogger(ProjectService.class);

	private ProjectRepository projectRepo;
	private AssignmentRepository assignmentRepo;

	public void createProject(Project project) {
		if (project.getId() == null || project.getId() == 0) {
			java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
			project.setDateCreated(date);
		}
		projectRepo.save(project);
	}

	public List<Project> findAllProjects() {
		return projectRepo.findAll();
	}

	public void deleteProject(Project project) {
		projectRepo.delete(project);
	}

	public List<Project> findProjectsByManager(Member manager) {
		return projectRepo.findAllByManager(manager);
	}

	public Project manageProject(Project project) {
		return projectRepo.findById(project.getId());
	}

	public List<Project> findProjectsByWorker(Member worker) {
		return projectRepo.findAllByWorker(worker);
	}

	public int countStoryPointsPerProject(Project project) {
		int totalValueInStoryPoints = 0;
		List<Assignment> projectsAssignments = assignmentRepo.findAllByProject(project);

		if (projectsAssignments != null) {
			logger.debug("Found project's(ID{}) assignments", project.getId());
			for (Assignment assignmentInPro : projectsAssignments) {
				totalValueInStoryPoints += assignmentInPro.getValueInStoryPoints();
				logger.info("Counted project's(ID{}) story points", project.getId());
			}
		}
		return totalValueInStoryPoints;
	}

	public int countDoneStoryPointsPerProject(Project project) {
		int valueInDoneStoryPoints = 0;
		List<Assignment> projectDoneAssignments = assignmentRepo.findByStatusInProject(AssignmentStatus.DONE, project);

		if (projectDoneAssignments != null) {
			logger.debug("Project(ID{}) has done assignments", project.getId());
			for (Assignment assignmentInPro : projectDoneAssignments) {
				valueInDoneStoryPoints += assignmentInPro.getValueInStoryPoints();
			}
		}
		return valueInDoneStoryPoints;
	}

	public int countNotDoneStoryPointsPerProject(Project project) {
		int valueInNotDoneStoryPoints = 0;
		List<Assignment> projectsAssignments = assignmentRepo.findAllByProject(project);

		if (projectsAssignments != null) {
			logger.debug("Project(ID{}) has not done assignments", project.getId());
			for (Assignment assignmentInPro : projectsAssignments) {
				if (assignmentInPro.getStatus() != AssignmentStatus.DONE) {
					valueInNotDoneStoryPoints += assignmentInPro.getValueInStoryPoints();
				}
			}
		}
		return valueInNotDoneStoryPoints;
	}

	public int countPercentageOfDonePerProject(Project project) {
		int totalValue = countStoryPointsPerProject(project);

		if (totalValue == 0) {
			logger.debug("Project(ID{}) has no assignments", project.getId());
			return 0;
		}

		int doneValue = countDoneStoryPointsPerProject(project);
		return (doneValue * 100) / totalValue;
	}

	public double countStoryPointsPerDayPerProject(Project project) {
		int doneStoryPoints = 0;
		int projectDays = 0;
		double storyPointsPerDay = 0;
		DateTime now = new DateTime();
		LocalDate today = now.toLocalDate();
		LocalDate projectStarts = LocalDate.fromDateFields(project.getStartDate());

		if (project.getEndDate() != null) {
			LocalDate projectEnds = LocalDate.fromDateFields(project.getEndDate());

			if (today.isAfter(projectEnds)) {
				logger.debug("Project(ID{}) is completed", project.getId());
				projectDays = Days.daysBetween(projectStarts, projectEnds).getDays();
			} 
			else {
				logger.debug("Project(ID{}) is not completed", project.getId());
				projectDays = Days.daysBetween(projectStarts, today).getDays();
			}
		}
		else {
			logger.debug("Project(ID{}) has no end date", project.getId());
			projectDays = Days.daysBetween(projectStarts, today).getDays();
		}
		
		if (projectDays != 0) {
			doneStoryPoints = countDoneStoryPointsPerProject(project);

			storyPointsPerDay = ((double) Math.round(1000 * (double) doneStoryPoints / (double) projectDays)) / 1000;

		}
		
		return storyPointsPerDay;
	}

	public int countDoneAssignmentsPerProject(Project project) {
		List<Assignment> projectDoneAssignments = assignmentRepo.findByStatusInProject(AssignmentStatus.DONE, project);

		if (projectDoneAssignments != null) {
			logger.debug("Project(ID{}) has done assignments{}", project.getId(), projectDoneAssignments.size());
			return projectDoneAssignments.size();
		}
		return 0;
	}

	public double countAssignmentsPerDayPerProject(Project project) {
		int doneAssignments = 0;
		int projectDays = 0;
		double assignmentsPerDay = 0;
		DateTime now = new DateTime();
		LocalDate today = now.toLocalDate();
		LocalDate projectStarts = LocalDate.fromDateFields(project.getStartDate());

		if (project.getEndDate() != null) {
			LocalDate projectEnds = LocalDate.fromDateFields(project.getEndDate());

			if (today.isAfter(projectEnds)) {
				logger.debug("Project(ID{}) is completed", project.getId());
				projectDays = Days.daysBetween(projectStarts, projectEnds).getDays();
			} 
			else {
				logger.debug("Project(ID{}) is not completed", project.getId());
				projectDays = Days.daysBetween(projectStarts, today).getDays();
			}
		}
		else {
			logger.debug("Project(ID{}) has no end date", project.getId());
			projectDays = Days.daysBetween(projectStarts, today).getDays();
		}
		
		if (projectDays != 0) {
			doneAssignments = countDoneAssignmentsPerProject(project);
			assignmentsPerDay = ((double) Math.round(1000 * (double) doneAssignments / (double) projectDays)) / 1000;
		}
		
		return assignmentsPerDay;
	}

	public int countAssignment(Project managedPro) {
		return projectRepo.countAssignment(managedPro, false, false);
	}

	public int countDoneAssignment(Project managedPro) {
		return projectRepo.countAssignment(managedPro, true, false);
	}

	public int countAssignmentsNotDone(Project managedPro) {
		return projectRepo.countAssignment(managedPro, false, true);
	}

	public int countManagedProjectsForASpecificMember(Member loggedMember) {
		return projectRepo.countManagedProjectsForASpecificMember(loggedMember);
	}

	public int countWorkingProjectsForASpecificMember(Member loggedMember) {
		return projectRepo.countWorkingProjectsForASpecificMember(loggedMember);
	}

	public ProjectRepository getProjectRepo() {
		return projectRepo;
	}

	public void setProjectRepo(ProjectRepository projectRepo) {
		this.projectRepo = projectRepo;
	}

	public AssignmentRepository getAssignmentRepo() {
		return assignmentRepo;
	}

	public void setAssignmentRepo(AssignmentRepository assignmentRepo) {
		this.assignmentRepo = assignmentRepo;
	}
}
