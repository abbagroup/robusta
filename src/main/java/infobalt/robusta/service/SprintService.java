package infobalt.robusta.service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Project;
import infobalt.robusta.entities.Sprint;
import infobalt.robusta.entities.Sprint.SprintStatus;
import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.entities.BurndownData;
import infobalt.robusta.repositories.AssignmentRepository;
import infobalt.robusta.repositories.SprintRepository;

public class SprintService {

	static final Logger logger = LoggerFactory.getLogger(SprintService.class);

	private SprintRepository sprintRepo;
	private AssignmentRepository assignmentRepo;

	public SprintRepository getSprintRepo() {
		return sprintRepo;
	}

	public void setSprintRepo(SprintRepository sprintRepo) {
		this.sprintRepo = sprintRepo;
	}

	public AssignmentRepository getAssignmentRepo() {
		return assignmentRepo;
	}

	public void setAssignmentRepo(AssignmentRepository assignmentRepo) {
		this.assignmentRepo = assignmentRepo;
	}

	public void saveSprint(Sprint sprint) {
		if (sprint.getId() == null || sprint.getId() == 0) {
			java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
			sprint.setDateCreated(date);
		}
		if (sprint.getStatus() == null) {
			sprint.setStatus(SprintStatus.NOTACTIVE);
		}
		sprintRepo.save(sprint);
	}

	public Sprint loadSprint(Sprint sprint) {
		return sprintRepo.findById(sprint.getId());
	}

	public List<Sprint> findAllSprints() {
		return sprintRepo.findAll();
	}

	public void activateSprint(Sprint sprint, Project project) {
		if (sprint.getStatus() == SprintStatus.ACTIVE) {
			logger.debug("Got sprint's(ID{}) status", sprint.getId());
			FacesMessage message = new FacesMessage("Already active");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return;
		}
		DateTime now = new DateTime();
		LocalDate today = now.toLocalDate();
		LocalDate sprintStarts = LocalDate.fromDateFields(sprint.getStartDate());
		if (today.isBefore(sprintStarts)) {
			FacesMessage message = new FacesMessage("Sorry. You cannot activate Sprint before Start Date. Change Start Date first or activate it on a day");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return;
		}
		if (isAnotherSprintActiveOrCloserToNow(sprint, project)) {
			FacesMessage message = new FacesMessage("Sorry. You cannot activate sprint because there are other sprints");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return;
		}
		sprint.setStatus(SprintStatus.ACTIVE);
		logger.debug("Set sprint's(ID{}) status ACTIVE", sprint.getId());
		sprintRepo.save(sprint);
		FacesMessage message = new FacesMessage("Sprint activated");
		FacesContext.getCurrentInstance().addMessage(null, message);

	}

	private boolean isAnotherSprintActiveOrCloserToNow(Sprint sprintToEvaluate, Project project) {
		List<Sprint> sprints = sprintRepo.findAllByProject(project);
		for (Sprint sprint : sprints) {
			if (sprint.getId() == sprintToEvaluate.getId()) {
				continue;
			}
			if (sprint.getStatus() == SprintStatus.ACTIVE) {
				return true;
			}
			DateTime startDate = new DateTime(sprint.getStartDate());
			if (startDate.isBeforeNow()) {
				continue;
			}
			if (sprint.getStartDate().before(sprintToEvaluate.getStartDate())) {
				return true;
			}
		}
		return false;
	}

	public void completeSprint(Sprint sprint) {
		if (sprint.getStatus() == SprintStatus.ACTIVE) {
			logger.debug("Got sprint's(ID{}) status", sprint.getId());
			sprint.setStatus(SprintStatus.COMPLETED);
			logger.debug("Set sprint's(ID{}) status COMPLETED", sprint.getId());
			sprintRepo.save(sprint);
			FacesMessage message = new FacesMessage("Sprint Finished!");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public void deleteSprint(Sprint sprint) {
		sprintRepo.delete(sprint);
	}

	public List<Sprint> findByProject(Project project) {
		return sprintRepo.findAllByProject(project);
	}

	public List<Sprint> findByAssignment(Assignment assignment) {
		return sprintRepo.findAllByAssignment(assignment);
	}

	public int countStoryPointsPerSprint(Sprint sprint) {
		int totalValueInStoryPoints = 0;
		List<Assignment> sprintAssignments = assignmentRepo.findAllBySprint(sprint);

		if (sprintAssignments != null) {
			logger.debug("Found sprint's(ID{}) assignments", sprint.getId());
			for (Assignment assignmentInSprint : sprintAssignments) {
				totalValueInStoryPoints += assignmentInSprint.getValueInStoryPoints();
				logger.info("Counted sprint's(ID{}) story points", sprint.getId());
			}
		}
		return totalValueInStoryPoints;
	}

	public int countDoneStoryPointsPerSprint(Sprint sprint) {
		int valueInDoneStoryPoints = 0;
		List<Assignment> sprintDoneAssignments = assignmentRepo.findByStatusInSprint(AssignmentStatus.DONE, sprint,
				false);

		if (sprintDoneAssignments != null) {
			logger.debug("Sprint's(ID{}) has done assignments", sprint.getId());
			for (Assignment assignmentInSprint : sprintDoneAssignments) {
				valueInDoneStoryPoints += assignmentInSprint.getValueInStoryPoints();
			}
		}
		return valueInDoneStoryPoints;
	}

	public int countNotDoneStoryPointsPerSprint(Sprint sprint) {
		int valueInNotDoneStoryPoints = 0;
		List<Assignment> sprintAssignments = assignmentRepo.findAllBySprint(sprint);

		if (sprintAssignments != null) {
			for (Assignment assignmentInSprint : sprintAssignments) {
				logger.debug("Sprint's(ID{}) has not done assignments", sprint.getId());
				if (assignmentInSprint.getStatus() != AssignmentStatus.DONE) {
					valueInNotDoneStoryPoints += assignmentInSprint.getValueInStoryPoints();
				}
			}
		}
		return valueInNotDoneStoryPoints;
	}

	public List<Assignment> loadAssignmentsByStatus(Sprint sprint, AssignmentStatus status, boolean limitedResults) {
		return assignmentRepo.findByStatusInSprint(status, sprint, limitedResults);
	}

	public List<Assignment> loadAssignmentsNotInSprint(Project project) {
		return assignmentRepo.findByProjectNotInSprint(project);
	}

	public int countSprintDays(Sprint sprint) {
		int sprintDays = 0;
		LocalDate sprintStarts = LocalDate.fromDateFields(sprint.getStartDate());
		LocalDate sprintEnds = LocalDate.fromDateFields(sprint.getEndDate());
		logger.debug("Got sprint's(ID{}) start and end dates", sprint.getId());
		sprintDays = Days.daysBetween(sprintStarts, sprintEnds).getDays();
		logger.info("Counted sprint's(ID{}) days", sprint.getId());
		return sprintDays;
	}

	private static int getCurrentSprintDay(Sprint sprint) {
		DateTime now = new DateTime();
		LocalDate today = now.toLocalDate();
		LocalDate sprintStarts = LocalDate.fromDateFields(sprint.getStartDate());
		int currentSprintDay = Days.daysBetween(sprintStarts, today).getDays();
		logger.debug("Got sprint's(ID{}) start date and counted the current day", sprint.getId());
		return currentSprintDay;
		
	} 
	
	/**
	 * When total amount of not done story points in sprint changes, method saves the new amount
	 * for that certain day when it was changed. If on that day the change has already happened,
	 * the left amount is updated. 
	 * @param sprint
	 */
	public void updateAndSaveBurndownHistory(Sprint sprint) {
		if (sprint.getStatus() != SprintStatus.COMPLETED) {
			int currentDay = getCurrentSprintDay(sprint);

			if (currentDay <= countSprintDays(sprint)) {
				boolean contains = false;
				for (BurndownData dayData : sprint.getBurndowData()) {
					if (currentDay == dayData.getDay()) {
						logger.debug("Sprint's(ID{}) data of today was updated", sprint.getId());
						dayData.setStoryPointsLeft(countNotDoneStoryPointsPerSprint(sprint));
						contains = true;
						sprintRepo.saveBurndownData(dayData);
						break;
					}
				}
				if (contains == false) {
					BurndownData dayData = new BurndownData();
					logger.debug("Sprint's(ID{}) data of today was created", sprint.getId());
					dayData.setDay(currentDay);
					dayData.setStoryPointsLeft(countNotDoneStoryPointsPerSprint(sprint));
					sprint.getBurndowData().add(dayData);
					dayData.setSprint(sprint);
					sprintRepo.saveBurndownData(dayData);
				}
			}
		}
		logger.debug("Sprint(ID{}) is completed, data is not updated", sprint.getId());
	}

	/** 
	 * Creates map of expecting burn-down data with two records where key is the first sprint day,  and
	 * the last sprint day, and value is the total amount of left to do story points for the first day record and the zero story points
	 * for the last day record.
	 * @param sprint
	 * @return data Map with two records according to sprint's data
	 */
	public Map<Object, Number> getExpectedBurndownData(Sprint sprint){
		Integer startingPoints = countStoryPointsPerSprint(sprint);
		Integer startingPeriod = countSprintDays(sprint);
		Map<Object, Number> expectedDataMap = new HashMap<Object, Number>();

		if (sprint.getStatus() == SprintStatus.NOTACTIVE) {
			logger.debug("Sprint(ID{}) is not active, burndown starting metrics can be changed", sprint.getId());
			expectedDataMap.put(Integer.valueOf(1), startingPoints);
			expectedDataMap.put(startingPeriod, Integer.valueOf(0));
			BurndownData oneRecord;

			if (sprint.getBurndowData().isEmpty()) {
				oneRecord = new BurndownData();
				oneRecord.setSprint(sprint);
				logger.debug("Sprint is set to its first data", sprint.getId());
				sprint.getBurndowData().add(oneRecord);
				logger.debug("First metric is added to sprint's(ID{}) data list", sprint.getId());
			} else {
				oneRecord = sprint.getBurndowData().get(0);
				logger.debug("Got first data metric of sprint(ID{})", sprint.getId());
			}

			oneRecord.setSprintPeriodInDays(startingPeriod);
			logger.debug("Set the period for started sprint(ID{})", sprint.getId());
			oneRecord.setTotalStoryPointsPerSprint(startingPoints);
			logger.debug("Set the total story points for started sprint(ID{})", sprint.getId());
			sprintRepo.saveBurndownData(oneRecord);
		} else {
			logger.debug("Sprint(ID{}) is active, burndown starting metrics are set and can not be changed",
					sprint.getId());
			expectedDataMap.put(Integer.valueOf(1), sprint.getBurndowData().get(0).getTotalStoryPointsPerSprint());
			expectedDataMap.put(sprint.getBurndowData().get(0).getSprintPeriodInDays(), Integer.valueOf(0));

		}
		return expectedDataMap;

	}
	
	/** 
	 * Puts all the saved burn-down chart data to a map, where key is the sprint day, on which
	 * changes happened, and the value is the amount of left to to story points 
	 * @param sprint
	 * @return data Map with records from the entity
	 */
	public Map<Object, Number> getSprintBurndownData(Sprint sprint) {
		List<BurndownData> sprintData = sprintRepo.findDataBySprint(sprint);
		Map<Object, Number> dataMap = new HashMap<Object, Number>();
		if (sprint.getStatus() == SprintStatus.NOTACTIVE) {
			dataMap.put(Integer.valueOf(1), Integer.valueOf(countStoryPointsPerSprint(sprint)));
		}
		if (sprint.getStatus() != SprintStatus.NOTACTIVE) {
			dataMap.put(Integer.valueOf(1), sprint.getBurndowData().get(0).getTotalStoryPointsPerSprint());
		}
		for (BurndownData oneRecord : sprintData) {
			Integer day = oneRecord.getDay();
			Integer points = oneRecord.getStoryPointsLeft();
			dataMap.put(day, points);
		}
		logger.debug("Sprint's(ID{}) actual data map is set with keys and values", sprint.getId());

		return dataMap;
	}

	public List<Assignment> loadUnassignedAssignmentsInSprint(Sprint sprint) {
		List<Assignment> assignments = assignmentRepo.findAllUnassignedAssignmentsInSprint(sprint);
		logger.debug("Loaded unassigned assignments({}) in sprint(ID{})", assignments.size(), sprint.getId());
		return assignmentRepo.findAllUnassignedAssignmentsInSprint(sprint);
	}

	public void removeAssignments(Sprint sprint) {

		List<Assignment> assignments = sprint.getSprintAssignments();
		for (Assignment assignment : assignments) {
			assignment.setSprint(null);
			assignmentRepo.save(assignment);
		}

		sprint.getSprintAssignments().removeAll(assignments);
		saveSprint(sprint);
	}

	public void removeBurndownData(Sprint sprint) {

		List<BurndownData> bdata = sprint.getBurndowData();
		for (BurndownData bd : bdata) {
			bd.setSprint(null);
			sprintRepo.saveBurndownData(bd);
		}

		saveSprint(sprint);
	}

	public Boolean canActivateSprint(Sprint sprint) {
		if (sprint.getStatus() == SprintStatus.ACTIVE || sprint.getStatus() == SprintStatus.COMPLETED) {
			return false;
		}
		DateTime now = new DateTime();
		LocalDate today = now.toLocalDate();
		LocalDate sprintStarts = LocalDate.fromDateFields(sprint.getStartDate());
		if (sprintStarts.isBefore(today)) {
			return false;
		}
		return true;
	}

	public List<Sprint> findByCurrentSprint(Project managedProject) {
		return sprintRepo.findCurrentSprintByProject(managedProject);
	}

	public Boolean canCompleteSprint(Sprint sprint) {
		if (sprint.getStatus() == SprintStatus.NOTACTIVE || sprint.getStatus() == SprintStatus.COMPLETED) {
			return false;
		}
		return true;
	}
}
