package infobalt.robusta.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;
import infobalt.robusta.entities.Sprint;
import infobalt.robusta.model.AssignmentSearchCriterias;
import infobalt.robusta.repositories.AssignmentRepository;

public class AssignmentService {

	private static final Logger logger = LoggerFactory.getLogger(AssignmentService.class);

	private AssignmentRepository assignmentRepo;

	public List<Assignment> findAssignmentsByAsignee(Member asignee) {
		return assignmentRepo.findAllByAsignee(asignee);
	}

	public List<Assignment> findAllByIds(List<Long> assignmentsIds) {
		return assignmentRepo.findAllByIds(assignmentsIds);
	}

	public List<Long> findAssignmentsIdListByProject(Project project) {
		List<Assignment> assignments = assignmentRepo.findAllByProject(project);
		List<Long> foundAssgnmentsId = new ArrayList<Long>();
		logger.info("Searched for assignments IDs in project(ID{})", project.getId());

		for (Assignment assignment : assignments) {
			foundAssgnmentsId.add(assignment.getId());
		}
		return foundAssgnmentsId;
	}

	public List<Long> findAssignmentsIdListBySprint(Sprint sprint) {
		List<Assignment> assignments = assignmentRepo.findAllBySprint(sprint);
		List<Long> foundAssgnmentsId = new ArrayList<Long>();
		logger.info("Searched for assignments IDs in sprint(ID{})", sprint.getId());

		for (Assignment assignment : assignments) {
			foundAssgnmentsId.add(assignment.getId());
		}
		return foundAssgnmentsId;
	}

	public List<Assignment> findAssignmentsBySprint(Sprint sprint) {
		return assignmentRepo.findAllBySprint(sprint);
	}

	public void save(Assignment assignment) {
		assignmentRepo.save(assignment);
	}

	public void delete(Assignment assignment) {
		assignment.setSprint(null);
		assignmentRepo.delete(assignment);
	}

	public List<Assignment> findAllByProject(Project managedPro) {
		return assignmentRepo.findAllByProject(managedPro);
	}

	public List<Assignment> findAssignmentsByCriterias(AssignmentSearchCriterias criterias, Long memberId) {
		return assignmentRepo.findAssignmentsByCriterias(criterias, memberId);
	}

	public List<Assignment> findLazyAssignmentsByProject(Project managedPro, int first, int maxResults,
			String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		return assignmentRepo.findAllLazyAssignmentsByProject(managedPro, first, maxResults, sortField, sortOrder,
				filters);
	}

	public int countTotalAssignmentsInProject(Project managedPro, Map<String, Object> filters) {
		return assignmentRepo.countTotalAssignmentsInProject(managedPro, filters);
	}

	public Assignment findAssingmentById(Long id) {
		return assignmentRepo.findAssingmentById(id);
	}

	public int countTotalAssignmentsByMember(Member loggedMember, Map<String, Object> filters) {
		return assignmentRepo.countTotalAssignmentsByMember(loggedMember, filters);
	}

	public List<Assignment> findLazyAssignmentsByMember(Member loggedMember, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		return assignmentRepo.findLazyAssignmentsByMember(loggedMember, first, pageSize, sortField, sortOrder, filters);
	}

	public Assignment findAssignmentByNameAndProjectId(String data, Long projectId) {
		return assignmentRepo.findAssignmentByNameAndProjectId(data, projectId);
	}

	public AssignmentRepository getAssignmentRepo() {
		return assignmentRepo;
	}

	public void setAssignmentRepo(AssignmentRepository assignmentRepo) {
		this.assignmentRepo = assignmentRepo;
	}
}
