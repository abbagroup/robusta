package infobalt.robusta.repositories;

import java.util.List;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.File;

public interface FileRepository {

	public void saveFile(File file);

	public void deleteFile(File file);

	public List<File> findAllFiles();

	public List<File> findFilesByAssignment(Assignment currentAssignment);
}
