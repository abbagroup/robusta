package infobalt.robusta.repositories;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;
import infobalt.robusta.entities.Sprint;
import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.model.AssignmentSearchCriterias;

public interface AssignmentRepository {

	public void save(Assignment newAssignment);

	public void delete(Assignment assignment);

	public List<Assignment> findAll();

	public Assignment findById(Long id);

	public List<Assignment> findAllByIds(List<Long> assignmentsIds);

	public List<Assignment> findAllByProject(Project currentProject);

	public List<Assignment> findAllByAsignee(Member asignee);

	public List<Assignment> findAllBySprint(Sprint sprint);

	public List<Assignment> findAssignmentsByCriterias(AssignmentSearchCriterias criterias, Long memberId);

	public List<Assignment> findByStatusInProject(AssignmentStatus status, Project project);

	public List<Assignment> findByStatusInSprint(AssignmentStatus status, Sprint sprint, boolean limitedResults);

	public List<Assignment> findByProjectNotInSprint(Project project);

	public List<Assignment> findAllLazyAssignmentsByProject(Project project, int startingAt, int maxPerPage,
			String sortField, SortOrder sortOrder, Map<String, Object> filters);

	public Assignment findAssingmentById(Long id);

	public List<Assignment> findLazyAssignmentsByMember(Member loggedMember, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters);

	public Assignment findAssignmentByNameAndProjectId(String data, Long projectId);

	public List<Assignment> findAllUnassignedAssignmentsInSprint(Sprint sprint);

	public int countTotalAssignmentsInProject(Project managedPro, Map<String, Object> filters);

	public int countTotalAssignmentsByMember(Member loggedMember, Map<String, Object> filters);

}
