package infobalt.robusta.repositories;

import java.util.List;

import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;

public interface MemberRepository {

	public void create(Member member);

	public void delete(Member member);

	public Member findById(Long id);

	public Member findByLogin(String login);

	public List<Member> findAllMembers();

	public List<Member> findByLoginAndPassword(String login, String password);

	public List<Member> findWorkersByProject(Project project);

	public List<Long> findWorkersIdsByProject(Project project);

}
