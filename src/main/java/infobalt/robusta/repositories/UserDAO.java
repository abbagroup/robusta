package infobalt.robusta.repositories;

import infobalt.robusta.entities.Member;

public interface UserDAO {
    public Member getUser(String login);
}
