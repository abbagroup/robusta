package infobalt.robusta.repositories;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Comment;

public interface CommentRepository {

	public void deleteComment(Comment comment);

	public void createComment(Comment comment);

	public List<Comment> findAllComments();

	public Comment findCommentById(Long id);

	public List<Comment> findCommentsByAssignment(Assignment currentAssignment);

	public int countTotalComments(Map<String, Object> filters);

	public List<Comment> findAllLazyComments(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters);

}
