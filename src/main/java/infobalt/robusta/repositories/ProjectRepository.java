package infobalt.robusta.repositories;

import java.util.List;

import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;

public interface ProjectRepository {

	public void save(Project newProject);

	public void delete(Project project);

	public List<Project> findAll();

	public Project findById(Long id);

	public List<Project> findAllByManager(Member projectManager);

	public List<Project> findAllByWorker(Member worker);

	public int countAssignment(Project project, boolean countDoneOnly, boolean countNotDoneOnly);

	public int countManagedProjectsForASpecificMember(Member loggedMember);

	public int countWorkingProjectsForASpecificMember(Member loggedMember);

}
