package infobalt.robusta.repositories;

import java.util.List;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.BurndownData;
import infobalt.robusta.entities.Project;
import infobalt.robusta.entities.Sprint;

public interface SprintRepository {

	public void save(Sprint newSprint);

	public void delete(Sprint sprint);

	public List<Sprint> findAll();

	public Sprint findById(Long id);

	public List<Sprint> findAllByProject(Project project);

	public List<Sprint> findAllByAssignment(Assignment assignment);

	public List<Sprint> findCurrentSprintByProject(Project managedProject);

	public void saveBurndownData(BurndownData burndownData);

	public void deleteBurndownData(BurndownData burndownData);

	public List<BurndownData> findDataBySprint(Sprint sprint);

	public BurndownData findDataById(Long id);

}
