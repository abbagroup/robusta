package infobalt.robusta.model;

import java.io.Serializable;
import java.util.List;

import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.entities.Sprint;

public class SprintModel implements Serializable {

	private static final long serialVersionUID = -6786506780014187120L;

	private Sprint sprint;

	private Sprint editedSprint;

	private List<Long> assignmentsId;

	private AssignmentStatus watchedAssignmentStatus;

	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

	public List<Long> getAssignmentsId() {
		return assignmentsId;
	}

	public void setAssignmentsId(List<Long> assignmentsId) {
		this.assignmentsId = assignmentsId;
	}

	public AssignmentStatus getWatchedAssignmentStatus() {
		return watchedAssignmentStatus;
	}

	public void setWatchedAssignmentStatus(AssignmentStatus watchedAssignmentStatus) {
		this.watchedAssignmentStatus = watchedAssignmentStatus;
	}

	public Sprint getEditedSprint() {
		return editedSprint;
	}

	public void setEditedSprint(Sprint editedSprint) {
		this.editedSprint = editedSprint;
	}

}
