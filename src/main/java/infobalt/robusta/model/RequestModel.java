package infobalt.robusta.model;

import java.util.List;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;
import infobalt.robusta.service.MemberService;
import infobalt.robusta.service.ProjectService;

public class RequestModel {

	private ProjectModel projectModel;

	private ProjectService projectService;

	private MemberService memberService;

	private MemberModel memberModel;

	private List<Long> workersIds;

	private String newPassword;

	private Project project;

	private Assignment assignment;

	public Project getProject() {
		return project;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public MemberModel getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberModel memberModel) {
		this.memberModel = memberModel;
	}

	public List<Member> getMembersAvailableForProject() {
		return memberService.findAllMembers();
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public List<Long> getWorkersIds() {
		if (workersIds == null) {
			Project managedPro = projectModel.getManagedPro();
			return memberService.findWorkersIdListByProject(managedPro);
		}
		return workersIds;
	}

	public void setWorkersIds(List<Long> workersIds) {
		this.workersIds = workersIds;
	}

	public Assignment getAssignment() {
		return assignment;
	}

	public void setAssignment(Assignment assignment) {
		this.assignment = assignment;
	}
}
