package infobalt.robusta.model;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.entities.Project;

public class AssignmentSearchCriterias {

	private Date creationFromDate;
	private Date creationTillDate;
	private String name;
	private String description;
	@Enumerated(EnumType.STRING)
	private AssignmentStatus status;
	private String sprintTitle;
	private Date editedFromDate;
	private Date editedTillDate;
	private Date doneFromDate;
	private Date doneTilDate;
	private Project specificProject;

	public Date getCreationFromDate() {
		return creationFromDate;
	}

	public void setCreationFromDate(Date creationFromDate) {
		this.creationFromDate = creationFromDate;
	}

	public Date getCreationTillDate() {
		return creationTillDate;
	}

	public void setCreationTillDate(Date creationTillDate) {
		this.creationTillDate = creationTillDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AssignmentStatus getStatus() {
		return status;
	}

	public void setStatus(AssignmentStatus status) {
		this.status = status;
	}

	public String getSprintTitle() {
		return sprintTitle;
	}

	public void setSprintTitle(String sprintTitle) {
		this.sprintTitle = sprintTitle;
	}

	public Date getEditedFromDate() {
		return editedFromDate;
	}

	public void setEditedFromDate(Date editedFromDate) {
		this.editedFromDate = editedFromDate;
	}

	public Date getEditedTillDate() {
		return editedTillDate;
	}

	public void setEditedTillDate(Date editedTillDate) {
		this.editedTillDate = editedTillDate;
	}

	public Date getDoneFromDate() {
		return doneFromDate;
	}

	public void setDoneFromDate(Date doneFromDate) {
		this.doneFromDate = doneFromDate;
	}

	public Date getDoneTilDate() {
		return doneTilDate;
	}

	public void setDoneTilDate(Date doneTilDate) {
		this.doneTilDate = doneTilDate;
	}

	public Project getSpecificProject() {
		return specificProject;
	}

	public void setSpecificProject(Project specificProject) {
		this.specificProject = specificProject;
	}
}
