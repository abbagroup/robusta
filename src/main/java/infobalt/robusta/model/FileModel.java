package infobalt.robusta.model;

import org.primefaces.model.StreamedContent;

public class FileModel {

	private StreamedContent downloadable;

	public StreamedContent getDownloadable() {
		return downloadable;
	}

	public void setDownloadable(StreamedContent downloadable) {
		this.downloadable = downloadable;
	}

}