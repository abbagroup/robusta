package infobalt.robusta.model;

import java.io.Serializable;

import infobalt.robusta.entities.Comment;

public class CommentModel implements Serializable {

	private static final long serialVersionUID = 4902075584468703567L;

	private Comment comment;

	private String commentText;

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

}
