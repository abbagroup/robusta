package infobalt.robusta.model;

import java.io.Serializable;
import java.util.List;

import infobalt.robusta.entities.Member;

public class MemberModel implements Serializable {

	private static final long serialVersionUID = 5620886201686881994L;

	private Member member;

	private Member loggedMember;

	private List<Member> selectedMembers;

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Member getLoggedMember() {
		return loggedMember;
	}

	public void setLoggedMember(Member loggedMember) {
		this.loggedMember = loggedMember;
	}

	public void setSelectedMembers(List<Member> selectedMembers) {
		this.selectedMembers = selectedMembers;
	}

	public List<Member> getSelectedMembers() {
		return selectedMembers;
	}

}
