package infobalt.robusta.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import infobalt.robusta.entities.Assignment;

public class AssignmentModel implements Serializable {

	private static final long serialVersionUID = -8131673305859876205L;

	private Long assigneeId;

	private boolean visible;

	@Valid
	private Assignment currentAssignment;

	private Assignment editedAssignment;

	private List<Assignment> foundAssignments;

	public boolean getVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public void init() {
		currentAssignment = new Assignment();
		foundAssignments = new ArrayList<Assignment>();
	}

	public List<Assignment> getFoundAssignments() {
		return foundAssignments;
	}

	public void setFoundAssignments(List<Assignment> foundAssignments) {
		this.foundAssignments = foundAssignments;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public Assignment getCurrentAssignment() {
		return currentAssignment;
	}

	public void setCurrentAssignment(Assignment currentAssignment) {
		this.currentAssignment = currentAssignment;
	}

	public Assignment getEditedAssignment() {
		return editedAssignment;
	}

	public void setEditedAssignment(Assignment editedAssignment) {
		this.editedAssignment = editedAssignment;
	}
}
