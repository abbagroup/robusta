package infobalt.robusta.model;

import java.io.Serializable;
import java.util.List;

import infobalt.robusta.entities.Project;

public class ProjectModel implements Serializable {

	private static final long serialVersionUID = -545003010467930645L;

	private Project project;

	private Project managedPro;

	private Long managerId;

	private List<Project> managedProjects;

	private List<Long> workersIds;

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
		if (project != null && project.getProjectManager() != null) {
			managerId = project.getProjectManager().getId();
		}
	}

	public Project getManagedPro() {
		return managedPro;
	}

	public void setManagedPro(Project managedPro) {
		this.managedPro = managedPro;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public List<Project> getManagedProjects() {
		return managedProjects;
	}

	public void setManagedProjects(List<Project> managedProjects) {
		this.managedProjects = managedProjects;
	}

	public List<Long> getWorkersIds() {
		return workersIds;
	}

	public void setWorkersIds(List<Long> workersIds) {
		this.workersIds = workersIds;
	}
}
