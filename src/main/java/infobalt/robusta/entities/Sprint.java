package infobalt.robusta.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
public class Sprint implements Serializable {

	private static final long serialVersionUID = 4152773773142730948L;

	public enum SprintStatus {
		NOTACTIVE("Not Active"), ACTIVE("Active"), COMPLETED("Completed");

		private final String label;

		private SprintStatus(String label) {
			this.label = label;
		}

		public String getLabel() {
			return this.label;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private SprintStatus status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreated;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	@NotNull
	private String name;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	@NotNull
	private Project project;

	@OneToMany(mappedBy = "sprint", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	private List<Assignment> sprintAssignments;

	@OneToMany(mappedBy = "sprint", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.PERSIST)
	private List<BurndownData> burndowData;

	public Sprint() {
		sprintAssignments = new ArrayList<Assignment>();
		burndowData = new ArrayList<BurndownData>();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public List<BurndownData> getBurndowData() {
		return burndowData;
	}

	public void setBurndowData(List<BurndownData> burndowData) {
		this.burndowData = burndowData;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<Assignment> getSprintAssignments() {
		return sprintAssignments;
	}

	public void setSprintAssignments(List<Assignment> sprintAssignments) {
		this.sprintAssignments = sprintAssignments;
	}

	public int countAssignments() {
		return sprintAssignments.size();
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public SprintStatus getStatus() {
		return status;
	}

	public void setStatus(SprintStatus status) {
		this.status = status;
	}

}
