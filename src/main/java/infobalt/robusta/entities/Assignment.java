package infobalt.robusta.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Assignment implements Serializable {

	private static final long serialVersionUID = -8401457450083000994L;

	public enum AssignmentStatus {
		TODO("To Do"), INPROGRESS("In Progress"), DONE("Done");

		private final String label;

		private AssignmentStatus(String label) {
			this.label = label;
		}

		public String getLabel() {
			return this.label;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	private Date doneDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "done_author_id")
	private Member doneAuthor;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	private Project project;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "creator_id")
	private Member creator;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sprint_id")
	private Sprint sprint;

	@Column
	private String description;

	@Column
	private int valueInStoryPoints;

	@Column
	private AssignmentStatus status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date editedDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "member_id")
	private Member assignee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Member getCreator() {
		return creator;
	}

	public void setCreator(Member creator) {
		this.creator = creator;
	}

	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getValueInStoryPoints() {
		return valueInStoryPoints;
	}

	public void setValueInStoryPoints(int valueInStoryPoints) {
		this.valueInStoryPoints = valueInStoryPoints;
	}

	public AssignmentStatus getStatus() {
		return status;
	}

	public void setStatus(AssignmentStatus status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getEditedDate() {
		return editedDate;
	}

	public void setEditedDate(Date editedDate) {
		this.editedDate = editedDate;
	}

	public Member getAssignee() {
		return assignee;
	}

	public void setAssignee(Member assignee) {
		this.assignee = assignee;
	}

	public Member getDoneAuthor() {
		return doneAuthor;
	}

	public Date getDoneDate() {
		return doneDate;
	}

	public void setDoneAuthor(Member doneAuthor) {
		this.doneAuthor = doneAuthor;
	}

	public void setDoneDate(Date doneDate) {
		this.doneDate = doneDate;
	}

	@Override
	public String toString() {
		return ("assignment id: " + id + " name: " + name);
	}

}
