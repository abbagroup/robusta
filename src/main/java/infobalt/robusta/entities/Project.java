package infobalt.robusta.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
public class Project implements Serializable {

	private static final long serialVersionUID = 8625807618624751468L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Date dateCreated;

	@NotNull
	private String name;

	@Temporal(TemporalType.DATE)
	@NotNull(message = "Project must have start date")
	private Date startDate;

	private String description;

	@Temporal(TemporalType.DATE)
	private Date endDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "manager_id")
	private Member projectManager;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "PROJECT_MEMBER", joinColumns = @JoinColumn(name = "WORKINGPROJECTS_ID") , inverseJoinColumns = @JoinColumn(name = "WORKERS_ID") )
	private List<Member> workers;

	@OneToMany(mappedBy = "project", orphanRemoval = true, fetch = FetchType.LAZY)
	private List<Assignment> assignments;

	@OneToMany(mappedBy = "project", orphanRemoval = true, fetch = FetchType.LAZY)
	private List<Sprint> sprints;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Member getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(Member manager) {
		this.projectManager = manager;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<Member> getWorkers() {
		return workers;
	}

	public void setWorkers(List<Member> workers) {
		this.workers = workers;
	}

	public List<Sprint> getSprints() {
		return sprints;
	}

	public void setSprints(List<Sprint> sprints) {
		this.sprints = sprints;
	}

	public List<Assignment> getAssignments() {
		return assignments;
	}

	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}

}
