package infobalt.robusta.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class BurndownData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sprint_id")
	private Sprint sprint;

	private Integer totalStoryPointsPerSprint;

	private Integer sprintPeriodInDays;

	private int storyPointsLeft;

	private int day;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

	public int getStoryPointsLeft() {
		return storyPointsLeft;
	}

	public void setStoryPointsLeft(int storyPointsLeft) {
		this.storyPointsLeft = storyPointsLeft;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public Integer getTotalStoryPointsPerSprint() {
		return totalStoryPointsPerSprint;
	}

	public void setTotalStoryPointsPerSprint(Integer totalStoryPointsPerSprint) {
		this.totalStoryPointsPerSprint = totalStoryPointsPerSprint;
	}

	public Integer getSprintPeriodInDays() {
		return sprintPeriodInDays;
	}

	public void setSprintPeriodInDays(Integer sprintPeriodInDays) {
		this.sprintPeriodInDays = sprintPeriodInDays;
	}

}
