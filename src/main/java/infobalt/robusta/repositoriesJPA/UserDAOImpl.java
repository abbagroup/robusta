package infobalt.robusta.repositoriesJPA;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Member;
import infobalt.robusta.repositories.UserDAO;

public class UserDAOImpl implements UserDAO {
	
	static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	private EntityManagerFactory entityManagerFactory;

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public Member getUser(String login) {

		List<Member> userList = new ArrayList<Member>();

		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Member> cq = cb.createQuery(Member.class);
			Root<Member> root = cq.from(Member.class);
			cq.where(cb.equal(root.get("loginName"), login));
			cq.select(root);
			TypedQuery<Member> q = entityManager.createQuery(cq);
			userList = q.getResultList();
			logger.info("Searched for user by login name: {}", login);
			if (userList.size() > 0){
				logger.debug("One or more user is found");
				return userList.get(0);
			}
			else
				return null;
		} finally {
			entityManager.close();
		}
	}

}
