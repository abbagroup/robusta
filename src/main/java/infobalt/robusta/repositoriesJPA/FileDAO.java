package infobalt.robusta.repositoriesJPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.File;
import infobalt.robusta.repositories.FileRepository;

public class FileDAO implements FileRepository {

	static final Logger logger = LoggerFactory.getLogger(FileDAO.class);

	private EntityManagerFactory entityManagerFactory;

	@Override
	public List<File> findAllFiles() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<File> cq = cb.createQuery(File.class);
			Root<File> root = cq.from(File.class);
			cq.select(root);
			TypedQuery<File> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void deleteFile(File file) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			file = entityManager.merge(file);
			entityManager.remove(file);
			entityManager.getTransaction().commit();
			logger.info("File(ID{}) is deleted", file.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void saveFile(File file) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(file)) {
				file = entityManager.merge(file);
			}
			entityManager.persist(file);
			entityManager.getTransaction().commit();
			logger.info("File(ID{}) is saved", file.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<File> findFilesByAssignment(Assignment currentAssignment) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<File> cq = cb.createQuery(File.class);
			Root<File> root = cq.from(File.class);
			cq.where(cb.equal(root.get("assignment"), currentAssignment));
			cq.select(root);
			TypedQuery<File> q = entityManager.createQuery(cq);
			logger.info("Searched assignment's(ID{}) files", currentAssignment.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}
