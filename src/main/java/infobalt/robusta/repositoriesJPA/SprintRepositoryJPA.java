package infobalt.robusta.repositoriesJPA;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.BurndownData;
import infobalt.robusta.entities.Project;
import infobalt.robusta.entities.Sprint;
import infobalt.robusta.entities.Sprint.SprintStatus;
import infobalt.robusta.repositories.SprintRepository;

public class SprintRepositoryJPA implements SprintRepository {

	private static final Logger logger = LoggerFactory.getLogger(SprintRepositoryJPA.class);

	private EntityManagerFactory entityManagerFactory;

	@Override
	public void save(Sprint newSprint) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(newSprint)) {
				newSprint = entityManager.merge(newSprint);
			} else {
				entityManager.persist(newSprint);
			}
			entityManager.getTransaction().commit();
			logger.info("Sprint(ID{}) is saved", newSprint.getId());
		} finally {
			entityManager.close();
		}

	}

	@Override
	public void delete(Sprint sprint) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			sprint = entityManager.merge(sprint);
			entityManager.remove(sprint);
			entityManager.getTransaction().commit();
			logger.info("Sprint(ID{}) is deleted", sprint.getId());
		} finally {
			entityManager.close();
		}

	}

	@Override
	public List<Sprint> findAll() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Sprint> cq = cb.createQuery(Sprint.class);
			Root<Sprint> root = cq.from(Sprint.class);
			cq.select(root);
			TypedQuery<Sprint> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Sprint findById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Sprint> cq = cb.createQuery(Sprint.class);
			Root<Sprint> root = cq.from(Sprint.class);
			cq.where(cb.equal(root.get("id"), id));
			TypedQuery<Sprint> q = entityManager.createQuery(cq);
			logger.info("Searched sprint by ID{}", id);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Sprint> findAllByProject(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			project = entityManager.merge(project);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Sprint> cq = cb.createQuery(Sprint.class);
			Root<Sprint> root = cq.from(Sprint.class);
			cq.select(root);
			cq.where(cb.equal(root.get("project"), project));
			TypedQuery<Sprint> q = entityManager.createQuery(cq);
			logger.info("Searched project's(ID{}) sprints", project.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Sprint> findAllByAssignment(Assignment assignment) {
		EntityManager entityManager = getEntityManager();
		String jpql = "SELECT s FROM Sprint s JOIN s.sprintAssignments a WHERE a.id = :searchAssignment";
		try {
			TypedQuery<Sprint> query = entityManager.createQuery(jpql, Sprint.class);
			query.setParameter("searchAssignment", assignment.getId());
			logger.info("Searched assignment's(ID{}) sprint", assignment.getId());
			return query.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void saveBurndownData(BurndownData burndownData) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(burndownData)) {
				burndownData = entityManager.merge(burndownData);
			} else {
				entityManager.persist(burndownData);
			}
			entityManager.getTransaction().commit();
			logger.info("Burndown data(ID{}) is saved", burndownData.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void deleteBurndownData(BurndownData burndownData) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			burndownData = entityManager.merge(burndownData);
			entityManager.remove(burndownData);
			entityManager.getTransaction().commit();
			logger.info("Burndown data(ID{}) is deleted", burndownData.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<BurndownData> findDataBySprint(Sprint sprint) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<BurndownData> cq = cb.createQuery(BurndownData.class);
			Root<BurndownData> root = cq.from(BurndownData.class);
			cq.where(cb.equal(root.get("sprint"), sprint));
			TypedQuery<BurndownData> q = entityManager.createQuery(cq);
			logger.info("Searched sprint's(ID{}) burndown data", sprint.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public BurndownData findDataById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<BurndownData> cq = cb.createQuery(BurndownData.class);
			Root<BurndownData> root = cq.from(BurndownData.class);
			cq.where(cb.equal(root.get("id"), id));
			TypedQuery<BurndownData> q = entityManager.createQuery(cq);
			logger.info("Searched burndown data by ID{}", id);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Sprint> findCurrentSprintByProject(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			project = entityManager.merge(project);
			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;

			Calendar dateNow = Calendar.getInstance();
			dateNow.setTime(new java.util.Date(Calendar.getInstance().getTimeInMillis()));
			Date dateToday = dateNow.getTime();

			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Sprint> cq = cb.createQuery(Sprint.class);
			Root<Sprint> root = cq.from(Sprint.class);
			cq.select(root);

			predicate = cb.equal(root.get("project"), project);
			criteria.add(predicate);

			predicate = cb.notEqual(root.get("status"), SprintStatus.COMPLETED);
			criteria.add(predicate);

			predicate = cb.lessThanOrEqualTo(root.get("startDate"), dateToday);
			criteria.add(predicate);

			predicate = cb.greaterThanOrEqualTo(root.get("endDate"), dateToday);
			criteria.add(predicate);

			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});

			cq.where(cb.and(predicateArray));
			TypedQuery<Sprint> q = entityManager.createQuery(cq);
			logger.info("Searched project's(ID{}) current sprints", project.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}
