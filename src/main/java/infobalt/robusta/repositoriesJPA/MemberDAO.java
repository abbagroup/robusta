package infobalt.robusta.repositoriesJPA;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;
import infobalt.robusta.repositories.MemberRepository;

public class MemberDAO implements MemberRepository {

	private static final Logger logger = LoggerFactory.getLogger(MemberDAO.class);

	private EntityManagerFactory entityManagerFactory;

	public void create(Member member) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(member)) {
				member = entityManager.merge(member);
			}
			entityManager.persist(member);
			entityManager.getTransaction().commit();
			logger.info("User(ID{}) is saved", member.getId());
		} finally {
			entityManager.close();
		}
	}

	public List<Member> findAllMembers() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Member> cq = cb.createQuery(Member.class);
			Root<Member> root = cq.from(Member.class);
			cq.where(cb.equal(root.get("role"), "user"));
			cq.orderBy(cb.asc(root.get("name")), cb.asc(root.get("loginName")));
			cq.select(root);
			TypedQuery<Member> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	public void delete(Member member) {
		EntityManager entityManager = getEntityManager();

		try {
			entityManager.getTransaction().begin();
			member = entityManager.merge(member);
			entityManager.remove(member);
			entityManager.getTransaction().commit();
			logger.info("User(ID{}) is deleted", member.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Member findById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Member> cq = cb.createQuery(Member.class);
			Root<Member> root = cq.from(Member.class);
			cq.where(cb.equal(root.get("id"), id));
			TypedQuery<Member> q = entityManager.createQuery(cq);
			logger.info("Searched user by ID{}", id);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Member> findByLoginAndPassword(String loginName, String password) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			List<Predicate> criteria = new ArrayList<Predicate>();
			CriteriaQuery<Member> cq = cb.createQuery(Member.class);
			Root<Member> root = cq.from(Member.class);
			cq.select(root);

			ParameterExpression<String> p1 = cb.parameter(String.class, "loginName");
			criteria.add(cb.equal(root.get("loginName"), p1));

			ParameterExpression<String> p2 = cb.parameter(String.class, "password");
			criteria.add(cb.equal(root.get("password"), p2));

			cq.where(cb.and(criteria.toArray(new Predicate[0])));
			TypedQuery<Member> q = entityManager.createQuery(cq);
			q.setParameter("loginName", loginName);
			q.setParameter("password", password);
			return q.getResultList();

		} finally {
			entityManager.close();
		}
	}

	@Override
	public Member findByLogin(String loginName) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			List<Predicate> criteria = new ArrayList<Predicate>();
			CriteriaQuery<Member> cq = cb.createQuery(Member.class);
			Root<Member> root = cq.from(Member.class);
			cq.select(root);
			ParameterExpression<String> p = cb.parameter(String.class, "loginName");
			criteria.add(cb.equal(root.get("loginName"), p));
			cq.where(cb.and(criteria.toArray(new Predicate[0])));
			TypedQuery<Member> q = entityManager.createQuery(cq);
			q.setParameter("loginName", loginName);
			logger.info("Searched user by loginName:{}", loginName);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Member> findWorkersByProject(Project project) {
		EntityManager entityManager = getEntityManager();
		String jpql = "SELECT p FROM Project m JOIN m.workers p WHERE m.id = :searchProject";

		try {
			TypedQuery<Member> query = entityManager.createQuery(jpql, Member.class);
			query.setParameter("searchProject", project.getId());
			logger.info("Searched for project's(ID{}) workers", project.getId());
			return query.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Long> findWorkersIdsByProject(Project project) {
		EntityManager entityManager = getEntityManager();
		String jpql = "SELECT p.id FROM Project m JOIN m.workers p WHERE m.id = :searchProject";
		try {
			TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
			query.setParameter("searchProject", project.getId());
			return query.getResultList();
		} finally {
			entityManager.close();
		}
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}
