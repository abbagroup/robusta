package infobalt.robusta.repositoriesJPA;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;
import infobalt.robusta.repositories.ProjectRepository;

public class ProjectRepositoryJPA implements ProjectRepository {

	private static final Logger logger = LoggerFactory.getLogger(ProjectRepositoryJPA.class);

	private EntityManagerFactory entityManagerFactory;

	@Override
	public void save(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(project)) {
				project = entityManager.merge(project);
			}
			entityManager.persist(project);
			entityManager.getTransaction().commit();
			logger.info("Project(ID{}) is saved", project.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void delete(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			project = entityManager.merge(project);
			entityManager.remove(project);
			entityManager.getTransaction().commit();
			logger.info("Project(ID{}) is deleted", project.getId());
		} finally {
			entityManager.close();
		}

	}

	@Override
	public List<Project> findAll() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Project> cq = cb.createQuery(Project.class);
			Root<Project> root = cq.from(Project.class);
			cq.select(root);
			TypedQuery<Project> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Project findById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Project> cq = cb.createQuery(Project.class);
			Root<Project> root = cq.from(Project.class);
			cq.where(cb.equal(root.get("id"), id));
			TypedQuery<Project> q = entityManager.createQuery(cq);
			logger.info("Searched project by ID{}", id);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Project> findAllByManager(Member projectManager) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Project> cq = cb.createQuery(Project.class);
			Root<Project> root = cq.from(Project.class);
			cq.select(root);
			cq.where(cb.equal(root.get("projectManager"), projectManager));
			TypedQuery<Project> q = entityManager.createQuery(cq);
			logger.info("Searched project manager's(ID{}) projects", projectManager.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Project> findAllByWorker(Member worker) {
		String jpql = "SELECT p FROM Project p JOIN p.workers w WHERE w.id = :searchWorker";

		EntityManager entityManager = getEntityManager();
		try {
			TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
			query.setParameter("searchWorker", worker.getId());
			logger.info("Searched user's(ID{}) projects", worker.getId());
			return query.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public int countAssignment(Project managedPro, boolean countDoneOnly, boolean countNotDoneOnly) {
		EntityManager entityManager = getEntityManager();
		try {
			managedPro = entityManager.merge(managedPro);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(cb.count(root));

			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;

			if (managedPro != null) {
				predicate = cb.equal(root.get("project"), managedPro);
				criteria.add(predicate);
			}
			if (countDoneOnly == true) {
				predicate = cb.equal(root.get("status"), AssignmentStatus.DONE);
				criteria.add(predicate);
			}

			if (countNotDoneOnly == true) {
				predicate = cb.notEqual(root.get("status"), AssignmentStatus.DONE);
				criteria.add(predicate);
			}

			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});

			cq.where(cb.and(predicateArray));
			TypedQuery<Long> q = entityManager.createQuery(cq);
			logger.info("Counted assigments of manager's project(ID{})", managedPro.getId());
			return q.getSingleResult().intValue();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public int countManagedProjectsForASpecificMember(Member loggedMember) {
		EntityManager entityManager = getEntityManager();
		try {
			loggedMember = entityManager.merge(loggedMember);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Project> root = cq.from(Project.class);
			cq.select(cb.count(root));

			cq.where(cb.equal(root.get("projectManager"), loggedMember));
			TypedQuery<Long> q = entityManager.createQuery(cq);
			logger.info("Counted manager's(ID{}) projects", loggedMember.getId());
			return q.getSingleResult().intValue();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public int countWorkingProjectsForASpecificMember(Member loggedMember) {
		EntityManager entityManager = getEntityManager();
		String jpql = "SELECT COUNT(p) FROM Project p JOIN p.workers w WHERE w.id = :searchWorker";
		try {
			TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
			query.setParameter("searchWorker", loggedMember.getId());
			logger.info("Counted user's(ID{}) projects", loggedMember.getId());
			return query.getSingleResult().intValue();
		} finally {
			entityManager.close();
		}
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}