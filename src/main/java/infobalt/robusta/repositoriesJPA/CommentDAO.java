package infobalt.robusta.repositoriesJPA;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Comment;
import infobalt.robusta.entities.Member;
import infobalt.robusta.repositories.CommentRepository;

public class CommentDAO implements CommentRepository {

	private static final Logger logger = LoggerFactory.getLogger(CommentDAO.class);

	private EntityManagerFactory entityManagerFactory;

	@Override
	public List<Comment> findAllComments() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Comment> cq = cb.createQuery(Comment.class);
			Root<Comment> root = cq.from(Comment.class);
			cq.select(root);
			TypedQuery<Comment> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Comment findCommentById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Comment> cq = cb.createQuery(Comment.class);
			Root<Comment> root = cq.from(Comment.class);
			cq.where(cb.equal(root.get("id"), id));
			TypedQuery<Comment> q = entityManager.createQuery(cq);
			logger.info("Searched comment by ID{}", id);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void deleteComment(Comment comment) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			comment = entityManager.merge(comment);
			entityManager.remove(comment);
			entityManager.getTransaction().commit();
			logger.info("Comment(ID{}) is deleted", comment.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void createComment(Comment comment) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(comment)) {
				comment = entityManager.merge(comment);
			}
			entityManager.persist(comment);
			entityManager.getTransaction().commit();
			logger.info("Comment(ID{}) is saved", comment.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Comment> findCommentsByAssignment(Assignment currentAssignment) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Comment> cq = cb.createQuery(Comment.class);
			Root<Comment> root = cq.from(Comment.class);
			cq.where(cb.equal(root.get("assignment"), currentAssignment));
			cq.orderBy(cb.asc(root.get("commentDate")));
			cq.select(root);
			TypedQuery<Comment> q = entityManager.createQuery(cq);
			logger.info("Searched assignment's(ID{}) comments", currentAssignment.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public int countTotalComments(Map<String, Object> filters) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Comment> root = cq.from(Comment.class);
			cq.select(cb.count(root));

			Predicate[] predicateArray = getPredicates(cb, root, filters);

			cq.where(cb.and(predicateArray));
			TypedQuery<Long> q = entityManager.createQuery(cq);
			logger.info("Counted comments");
			return q.getSingleResult().intValue();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Comment> findAllLazyComments(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Comment> cq = cb.createQuery(Comment.class);
			Root<Comment> root = cq.from(Comment.class);
			cq.select(root);

			Predicate[] predicateArray = getPredicates(cb, root, filters);

			cq.where(cb.and(predicateArray));
			if (sortField != null) {
				if (sortOrder.equals(SortOrder.ASCENDING)) {
					cq.orderBy(cb.asc(root.get(sortField)));
				} else {
					cq.orderBy(cb.desc(root.get(sortField)));
				}
			}
			TypedQuery<Comment> q = entityManager.createQuery(cq);
			q.setFirstResult(first);
			q.setMaxResults(pageSize);
			logger.info("Completed lazy comments loading");
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	public Predicate[] getPredicates(CriteriaBuilder cb, Root<Comment> root, Map<String, Object> filters) {

		List<Predicate> criteria = new ArrayList<Predicate>();
		Predicate predicate = null;

		if (filters.containsKey("id")) {
				predicate = cb.equal(root.get("id"), filters.get("id"));
				criteria.add(predicate);
		}

		if (filters.containsKey("author.getFullName()")) {
			Join<Comment, Member> memberJoin = root.join("author");
			predicate = cb.like(cb.concat(cb.concat(cb.lower(memberJoin.get("name")), " "), cb.lower(memberJoin.get("surname"))),
					"%" + filters.get("author.getFullName()").toString().toLowerCase() + "%");
			criteria.add(predicate);
		}

		if (filters.containsKey("commentText")) {
			predicate = cb.like(cb.lower(root.get("commentText")),
					"%" + filters.get("commentText").toString().toLowerCase() + "%");
			criteria.add(predicate);
		}

		if (filters.containsKey("creationDateFrom")) {
			predicate = cb.greaterThanOrEqualTo(root.get("commentDate"), (Date) filters.get("creationDateFrom"));
			criteria.add(predicate);
		}

		if (filters.containsKey("creationDateTo")) {
			predicate = cb.lessThanOrEqualTo(root.get("commentDate"), (Date) filters.get("creationDateTo"));
			criteria.add(predicate);
		}

		Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
		return predicateArray;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}
