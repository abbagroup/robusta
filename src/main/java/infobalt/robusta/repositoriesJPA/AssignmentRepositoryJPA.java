package infobalt.robusta.repositoriesJPA;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Assignment.AssignmentStatus;
import infobalt.robusta.model.AssignmentSearchCriterias;
import infobalt.robusta.entities.Member;
import infobalt.robusta.entities.Project;
import infobalt.robusta.entities.Sprint;
import infobalt.robusta.repositories.AssignmentRepository;

public class AssignmentRepositoryJPA implements AssignmentRepository {

	private static final Logger logger = LoggerFactory.getLogger(AssignmentRepositoryJPA.class);

	private EntityManagerFactory entityManagerFactory;

	@Override
	public void save(Assignment assignment) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(assignment) && assignment.getId() != null) {
				assignment = entityManager.merge(assignment);
			}
			entityManager.persist(assignment);
			entityManager.getTransaction().commit();
			logger.info("Assignment(ID{}) is saved", assignment.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void delete(Assignment assignment) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			assignment = entityManager.merge(assignment);
			entityManager.remove(assignment);
			entityManager.getTransaction().commit();
			logger.info("Assignment(ID{}) is deleted", assignment.getId());
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAll() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAllByProject(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			project = entityManager.merge(project);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);
			cq.where(cb.equal(root.get("project"), project));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			logger.info("Searched project's(ID{}) assignments", project.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}


	@Override
	public List<Assignment> findAllByAsignee(Member asignee) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);
			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;
			if (asignee != null) {
				predicate = cb.equal(root.get("assignee"), asignee);
				criteria.add(predicate);
				logger.debug("Searched in user's(ID{}) projects", asignee.getId());
				Join<Assignment, Project> assProJoin = root.join("project");
				predicate = cb.equal(assProJoin.join("workers").get("id"), asignee.getId());
				criteria.add(predicate);
			}
			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
			cq.where(cb.and(predicateArray));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			logger.info("Searched user's(ID{}) assignments", asignee.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Assignment findById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.where(cb.equal(root.get("id"), id));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			logger.info("Searched assignment by ID{}", id);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAllByIds(List<Long> assignmentsIds) {
		EntityManager entityManager = getEntityManager();
		String jpql = "SELECT a FROM Assignment a WHERE a.id IN :searchAssignments";
		try {
			TypedQuery<Assignment> query = entityManager.createQuery(jpql, Assignment.class);
			query.setParameter("searchAssignments", assignmentsIds);
			return query.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAllBySprint(Sprint sprint) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);
			cq.where(cb.equal(root.get("sprint"), sprint));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			logger.info("Searched sprint's(ID{}) assignments", sprint.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAssignmentsByCriterias(AssignmentSearchCriterias criterias, Long memberId) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> assRoot = cq.from(Assignment.class);
			cq.select(assRoot);

			if (criterias.getSpecificProject() != null) {
				Join<Assignment, Project> assProJoin = assRoot.join("project");
				predicate = cb.equal(assProJoin.get("id"), criterias.getSpecificProject().getId());
				criteria.add(predicate);
			}

			if (memberId != null) {
				logger.debug("Searched in user's(ID{}) projects", memberId);
				Join<Assignment, Project> assProJoin = assRoot.join("project");
				predicate = cb.equal(assProJoin.join("workers").get("id"), memberId);
				criteria.add(predicate);
			}

			if (criterias.getSprintTitle().isEmpty() == false) {
				logger.debug("Sprint name is in search criterias");
				Join<Assignment, Sprint> sprintJoin = assRoot.join("sprint");
				predicate = cb.like(cb.lower(sprintJoin.get("name")),
						"%" + criterias.getSprintTitle().toLowerCase() + "%");
				criteria.add(predicate);
			}

			if (criterias.getName() != null) {
				logger.debug("Assignment title (fragment) is in search criterias");
				predicate = cb.like(cb.lower(assRoot.get("name")), "%" + criterias.getName().toLowerCase() + "%");
				criteria.add(predicate);
			}

			if (criterias.getDescription() != null) {
				logger.debug("Assignment description (fragment) is in search criterias");
				predicate = cb.like(cb.lower(assRoot.get("description")),
						"%" + criterias.getDescription().toLowerCase() + "%");
				criteria.add(predicate);
			}

			if (criterias.getStatus() != null) {
				logger.debug("Assignment status is in search criterias");
				predicate = cb.equal(assRoot.get("status"), criterias.getStatus());
				criteria.add(predicate);
			}

			if (criterias.getCreationFromDate() != null) {
				logger.debug("Cretion date 'from' is in search criterias");
				predicate = cb.greaterThanOrEqualTo(assRoot.get("creationDate"), criterias.getCreationFromDate());
				criteria.add(predicate);
			}

			if (criterias.getCreationTillDate() != null) {
				logger.debug("Creation date 'till' is in search criterias");
				predicate = cb.lessThanOrEqualTo(assRoot.get("creationDate"), criterias.getCreationTillDate());
				criteria.add(predicate);
			}

			if (criterias.getDoneFromDate() != null) {
				logger.debug("DONE date 'from' is in search criterias");
				predicate = cb.greaterThanOrEqualTo(assRoot.get("doneDate"), criterias.getDoneFromDate());
				criteria.add(predicate);
			}

			if (criterias.getDoneTilDate() != null) {
				logger.debug("DONE date 'till' is in search criterias");
				predicate = cb.lessThanOrEqualTo(assRoot.get("doneDate"), criterias.getDoneTilDate());
				criteria.add(predicate);
			}

			if (criterias.getEditedFromDate() != null) {
				logger.debug("Editing date 'from' in search criterias");
				predicate = cb.greaterThanOrEqualTo(assRoot.get("editedDate"), criterias.getEditedFromDate());
				criteria.add(predicate);
			}

			if (criterias.getEditedTillDate() != null) {
				logger.debug("Editing date 'till' is in search criterias");
				predicate = cb.lessThanOrEqualTo(assRoot.get("editedDate"), criterias.getEditedTillDate());
				criteria.add(predicate);
			}

			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
			cq.where(cb.and(predicateArray));
			cq.orderBy(cb.desc(assRoot.get("creationDate")));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			logger.info("Searched by given criterias and ordered by date the result");
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findByStatusInProject(AssignmentStatus status, Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> assRoot = cq.from(Assignment.class);
			cq.select(assRoot);

			predicate = cb.equal(assRoot.get("status"), status);
			criteria.add(predicate);

			predicate = cb.equal(assRoot.get("project"), project);
			criteria.add(predicate);

			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
			cq.where(cb.and(predicateArray));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			logger.info("Searched for assignments in project(ID{}) by given status", project.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findByStatusInSprint(AssignmentStatus status, Sprint sprint, boolean limitedResults) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> assRoot = cq.from(Assignment.class);
			cq.select(assRoot);

			predicate = cb.equal(assRoot.get("status"), status);
			criteria.add(predicate);

			predicate = cb.equal(assRoot.get("sprint"), sprint);
			criteria.add(predicate);

			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
			cq.where(cb.and(predicateArray));
			cq.orderBy(cb.desc(assRoot.get("editedDate")));

			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			logger.info("Searched for assignments in sprint(ID{}) by given status", sprint.getId());
			if (limitedResults == true) {
				q.setMaxResults(10);
			}
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findByProjectNotInSprint(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> assRoot = cq.from(Assignment.class);
			cq.select(assRoot);

			predicate = cb.equal(assRoot.get("project"), project);
			criteria.add(predicate);

			predicate = cb.isNull(assRoot.get("sprint"));
			criteria.add(predicate);

			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
			cq.where(cb.and(predicateArray));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			logger.info("Searched for assignments in project(ID{}) with no sprint", project.getId());
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public int countTotalAssignmentsInProject(Project project, Map<String, Object> filters) {
		EntityManager entityManager = getEntityManager();
		try {
			project = entityManager.merge(project);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(cb.count(root));

			Predicate[] predicateArray = getPredicates(cb, root, project, filters, null);

			cq.where(cb.and(predicateArray));
			TypedQuery<Long> q = entityManager.createQuery(cq);
			return q.getSingleResult().intValue();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAllLazyAssignmentsByProject(Project project, int startingAt, int maxPerPage,
			String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		EntityManager entityManager = getEntityManager();
		try {
			project = entityManager.merge(project);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);

			Predicate[] predicateArray = getPredicates(cb, root, project, filters, null);

			cq.where(cb.and(predicateArray));
			if (sortField != null) {
				if (sortOrder.equals(SortOrder.ASCENDING)) {
					cq.orderBy(cb.asc(root.get(sortField)));
				} else {
					cq.orderBy(cb.desc(root.get(sortField)));
				}
			}
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			q.setFirstResult(startingAt);
			q.setMaxResults(maxPerPage);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Assignment findAssingmentById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.where(cb.equal(root.get("id"), id));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findLazyAssignmentsByMember(Member loggedMember, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		EntityManager entityManager = getEntityManager();
		try {
			loggedMember = entityManager.merge(loggedMember);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);

			Predicate[] predicateArray = getPredicates(cb, root, null, filters, loggedMember);

			cq.where(cb.and(predicateArray));
			if (sortField != null) {
				if (sortOrder.equals(SortOrder.ASCENDING)) {
					cq.orderBy(cb.asc(root.get(sortField)));
				} else {
					cq.orderBy(cb.desc(root.get(sortField)));
				}
			}
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			q.setFirstResult(first);
			q.setMaxResults(pageSize);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	public Predicate[] getPredicates(CriteriaBuilder cb, Root<Assignment> root, Project project,
			Map<String, Object> filters, Member member) {

		List<Predicate> criteria = new ArrayList<Predicate>();
		Predicate predicate = null;

		String idFilter = (String) filters.get("id");
		String nameFilter = (String) filters.get("name");
		String descriptionFilter = (String) filters.get("description");
		String storyPointsFilter = (String) filters.get("valueInStoryPoints");
		String sprintFilter = (String) (filters.get("sprint.name"));
		String statusFilter = (String) (filters.get("status"));
		String assigneeFilter = (String) (filters.get("assignee.getFullName()"));

		if (member != null) {
			logger.debug("Searched in user's(ID{}) projects", member.getId());
			Join<Assignment, Project> assProJoin = root.join("project");
			predicate = cb.equal(assProJoin.join("workers").get("id"), member.getId());
			criteria.add(predicate);
		}
		
		if (project != null) {
			predicate = cb.equal(root.get("project"), project);
			criteria.add(predicate);
		}
		if (member != null) {
			Join<Assignment, Member> memberJoin = root.join("assignee");
			predicate = cb.equal((memberJoin.get("id")), member.getId());
			criteria.add(predicate);
		}

		if (idFilter != null) {
			predicate = cb.equal(root.get("id"), filters.get("id"));
			criteria.add(predicate);
		}

		if (nameFilter != null) {
			predicate = cb.like(cb.lower(root.get("name")), "%" + filters.get("name").toString().toLowerCase() + "%");
			criteria.add(predicate);
		}

		if (descriptionFilter != null) {
			predicate = cb.like(cb.lower(root.get("description")),
					"%" + filters.get("description").toString().toLowerCase() + "%");
			criteria.add(predicate);
		}

		if (storyPointsFilter != null) {
			predicate = cb.equal(root.get("valueInStoryPoints"), filters.get("valueInStoryPoints"));
			criteria.add(predicate);
		}

		if (sprintFilter != null) {
			Join<Assignment, Sprint> sprintJoin = root.join("sprint");
			predicate = cb.like(cb.lower(sprintJoin.get("name")),
					"%" + filters.get("sprint.name").toString().toLowerCase() + "%");
			criteria.add(predicate);
		}

		if (assigneeFilter != null) {
			Join<Assignment, Member> memberJoin = root.join("assignee");
			predicate = cb.like(cb.concat(cb.concat(cb.lower(memberJoin.get("name")), " "), cb.lower(memberJoin.get("surname"))),
					"%" + filters.get("assignee.getFullName()").toString().toLowerCase() + "%");
			criteria.add(predicate);
		}

		if (statusFilter != null) {
			predicate = cb.equal((root.get("status")), AssignmentStatus.valueOf(statusFilter));
			criteria.add(predicate);

		}
		if (filters.containsKey("creationDateFrom")) {
			predicate = cb.greaterThanOrEqualTo(root.get("creationDate"), (Date) filters.get("creationDateFrom"));
			criteria.add(predicate);
		}

		if (filters.containsKey("creationDateTo")) {
			predicate = cb.lessThanOrEqualTo(root.get("creationDate"), (Date) filters.get("creationDateTo"));
			criteria.add(predicate);
		}

		if (filters.containsKey("editedDateFrom")) {
			predicate = cb.greaterThanOrEqualTo(root.get("editedDate"), (Date) filters.get("editedDateFrom"));
			criteria.add(predicate);
		}

		if (filters.containsKey("editedDateTo")) {
			predicate = cb.lessThanOrEqualTo(root.get("editedDate"), (Date) filters.get("editedDateTo"));
			criteria.add(predicate);
		}

		Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
		return predicateArray;
	}

	@Override
	public int countTotalAssignmentsByMember(Member loggedMember, Map<String, Object> filters) {
		EntityManager entityManager = getEntityManager();
		try {
			loggedMember = entityManager.merge(loggedMember);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(cb.count(root));

			Predicate[] predicateArray = getPredicates(cb, root, null, filters, loggedMember);

			cq.where(cb.and(predicateArray));
			TypedQuery<Long> q = entityManager.createQuery(cq);
			logger.info("Counted user's(ID{})assignments", loggedMember.getId());
			return q.getSingleResult().intValue();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Assignment findAssignmentByNameAndProjectId(String data, Long projectId) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);

			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;

			if (!data.isEmpty()) {
				predicate = cb.equal(root.get("name"), data);
				criteria.add(predicate);
			}

			if (projectId != null) {
				Join<Assignment, Project> projectJoin = root.join("project");
				predicate = cb.equal((projectJoin.get("id")), projectId);
				criteria.add(predicate);
			}

			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
			cq.where(cb.and(predicateArray));

			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			try {
				return q.getSingleResult();
			} catch (javax.persistence.NoResultException e) {
				return null;
			}
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAllUnassignedAssignmentsInSprint(Sprint sprint) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			List<Predicate> criteria = new ArrayList<Predicate>();
			Predicate predicate = null;
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> assRoot = cq.from(Assignment.class);
			cq.select(assRoot);

			predicate = cb.isNull(assRoot.get("assignee"));
			criteria.add(predicate);

			Join<Assignment, Sprint> sprintJoin = assRoot.join("sprint");
			predicate = cb.equal(sprintJoin.get("name"), sprint.getName());
			criteria.add(predicate);

			Predicate[] predicateArray = criteria.toArray(new Predicate[] {});
			cq.where(cb.and(predicateArray));
			try {
				TypedQuery<Assignment> q = entityManager.createQuery(cq);
				logger.info("Searched for unassigned assignments in Sprint(ID{})", sprint.getId());
				return q.getResultList();
			} catch (NullPointerException e) {
				logger.info("Sprint(ID{}) has zero unassigned assingments", sprint.getId());
				return null;
			}
		} finally {
			entityManager.close();
		}
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
}
