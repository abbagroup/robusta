package infobalt.robusta.validators;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SprintEndValidator implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(SprintEndValidator.class);

	@Override
	/**
	 * Validates if sprint's end date is set after the sprint's start date
	 */
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		validateValueAsDate(value);

		Date inputDate = (Date) value;
		DateTime inputDateTime = new DateTime(inputDate);

		UIInput startDateComponent = (UIInput) component.getAttributes().get("startDateComponent");
		Date startDate = (Date) startDateComponent.getValue();
		if (startDate == null) {
			return;
		}

		DateTime startDateTime = new DateTime(startDate);
		if (inputDateTime.isBefore(startDateTime)) {
			FacesMessage message = new FacesMessage("End date must be later than Start date");
			logger.error("Entered sprint end date is before sprint start date");
			throw new ValidatorException(message);
		}
	}

	private void validateValueAsDate(Object value) {
		if (!(value instanceof Date)) {
			FacesMessage message = new FacesMessage("Not a valid date");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			logger.error("Entered value for sprint is not Date type");
			throw new ValidatorException(message);
		}
	}
}
