package infobalt.robusta.validators;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Project;
import infobalt.robusta.ui.controllers.SprintController;

public class SprintInProjectDatesValidator implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(SprintInProjectDatesValidator.class);

	private SprintController sprintController;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		if (!(value instanceof Date)) {
			FacesMessage message = new FacesMessage("Not a valid date");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			logger.error("Entered value for sprint is not Date type");
			throw new ValidatorException(message);
		}

		Date endDate = (Date) value;

		UIInput startDateComponent = (UIInput) component.getAttributes().get("startDateComponent");
		Date startDate = (Date) startDateComponent.getValue();

		Project project = sprintController.loadCurrentProject();
		logger.debug("Validated project ID is not null");

		if (startDate != null && startDate.before(project.getStartDate())) {
			FacesMessage message = new FacesMessage(
					"Can't start Sprint before starting project. Project start: " + project.getStartDate());
			logger.error("Entered sprint start date is scheduled before project start date");
			throw new ValidatorException(message);
		}

		if (project.getEndDate() == null) {
			logger.debug("Project has no deadline, so any sprint end date is acceptable for this project");
			return;
		}

		if (endDate != null && endDate.after(project.getEndDate())) {
			FacesMessage message = new FacesMessage(
					"Can't end Sprint after project ends. Project deadline: " + project.getEndDate());
			logger.error("Entered sprint end date is scheduled after project end date");
			throw new ValidatorException(message);
		}
	}

	public SprintController getSprintController() {
		return sprintController;
	}

	public void setSprintController(SprintController sprintController) {
		this.sprintController = sprintController;
	}
}
