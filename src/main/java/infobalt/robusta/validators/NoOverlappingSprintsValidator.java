package infobalt.robusta.validators;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Sprint;
import infobalt.robusta.entities.Sprint.SprintStatus;
import infobalt.robusta.ui.controllers.SprintController;

public class NoOverlappingSprintsValidator implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(NoOverlappingSprintsValidator.class);

	private SprintController sprintController;

	@Override
	/**
	 * Validates if entered value for sprint dates is of Date type and if
	 * created sprint is not inside or overlap with another sprint created
	 */
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		if (!(value instanceof Date)) {
			FacesMessage message = new FacesMessage("Not a valid date");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			logger.error("Entered value for sprint is not Date type");
			throw new ValidatorException(message);
		}

		Date endDate = (Date) value;

		UIInput startDateComponent = (UIInput) component.getAttributes().get("startDateComponent");
		Date startDate = (Date) startDateComponent.getValue();

		List<Sprint> sprints = sprintController.loadProjectSprints();

		Sprint currentSprint = sprintController.getSprintModel().getEditedSprint();
		long currentSprintId = 0;
		if (currentSprint != null && currentSprint.getId() != null) {
			logger.debug("Validated sprint ID{} is not null", currentSprint.getId());
			currentSprintId = currentSprint.getId();
		}

		for (Sprint sprint : sprints) {

			if (currentSprintId != 0 && currentSprintId == sprint.getId()) {
				continue;
			}

			if (!(sprint.getStatus().equals(SprintStatus.COMPLETED))) {

				if (startDate != null && !startDate.before(sprint.getStartDate())
						&& !startDate.after(sprint.getEndDate())) {
					FacesMessage message = new FacesMessage(
							"Can not start sprint during another sprint. Choose a different start date");
					logger.error("Entered sprint start date is in the period of another sprint");
					throw new ValidatorException(message);
				}

				if (endDate != null && !endDate.before(sprint.getStartDate()) && !endDate.after(sprint.getEndDate())) {
					FacesMessage message = new FacesMessage(
							"Can not continue sprint during another sprint. Choose a different end date");
					logger.error("Entered sprint end date is in the period of another sprint");
					throw new ValidatorException(message);
				}

				if (startDate != null && endDate != null) {
					if (!startDate.after(sprint.getStartDate()) && !endDate.before(sprint.getEndDate())) {
						FacesMessage message = new FacesMessage(
								"There is another sprint scheduled within this date range. You can't sprint while you sprint");
						logger.error("Entered sprint period is in the period of another sprint");
						throw new ValidatorException(message);
					}
				}
			}
		}
	}

	public SprintController getSprintController() {
		return sprintController;
	}

	public void setSprintController(SprintController sprintController) {
		this.sprintController = sprintController;
	}

}
