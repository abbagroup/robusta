package infobalt.robusta.validators;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SprintStartValidator implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(SprintStartValidator.class);

	@Override
	/**
	 * Validates if sprint's start date is set after sprint creation date.
	 * Sprint can not start before it was created.
	 */
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		validateValueAsDate(value);

		Date inputDate = (Date) value;
		DateTime inputDateTime = new DateTime(inputDate);

		DateTime today = new DateTime();
		DateTime todayASecondLater = today.plusSeconds(1).withMillisOfDay(0);

		if (inputDateTime.isBefore(todayASecondLater)) {
			FacesMessage message = new FacesMessage(
					"Sprint can only start after it has been created. Enter today or a day after");
			logger.error("Entered sprint start date is before sprint was created date");
			throw new ValidatorException(message);
		}
	}

	private void validateValueAsDate(Object value) {
		if (!(value instanceof Date)) {
			FacesMessage message = new FacesMessage("Not a valid date");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			logger.error("Entered value for sprint is not Date type");
			throw new ValidatorException(message);
		}
	}
}
